"""References.

From::

    /home/niflheim2/mohpa/ref_sys_new_setups/PBE/complete/
"""

from collections import Counter

from ase.db import connect
from ase.io import read
from gpaw import GPAW, PW, FermiDirac


refname = __file__.rsplit('.', 1)[0] + '.json'


def read_references(filename=None):
    filename = filename or refname
    dct = {}
    db = connect(filename)
    for row in db.select():
        dct[row.symbols[0]] = row.energy / row.natoms
    return dct


def formation_energy(atoms, references):
    count = Counter(atoms.get_chemical_symbols())
    e0 = sum(n * references[symbol] for symbol, n in count.items())
    return atoms.get_potential_energy() - e0


def calculate_references(systems, vacuum=7.5, kpt_density=6.0,
                         width=0.05, ecut=800, xc='PBE'):
    db = connect('new-references.json')
    for atoms in systems:
        id = db.reserve(Z=atoms.numbers[0])
        if id:
            if not atoms.pbc.any():
                atoms.center(vacuum=vacuum)
            name = atoms.get_chemical_formula()
            atoms.calc = GPAW(mode=PW(ecut),
                              xc=xc,
                              kpts={'density': kpt_density},
                              txt=name + '.txt',
                              occupations=FermiDirac(width=width))
            atoms.get_potential_energy()
            atoms.get_forces()
            atoms.get_stress()
            atoms.calc.write(name + '.gpw')
            db.write(atoms,
                     symbol=atoms.get_chemical_symbols()[0],
                     Z=atoms.numbers[0])
            del db[id]


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('filename')
    parser.add_argument('references', nargs='?')
    args = parser.parse_args()
    atoms = read(args.filename)
    references = read_references(args.references)
    fe = formation_energy(atoms, references)
    count = Counter(atoms.get_chemical_symbols())
    ref = ' - '.join('{}*E({})'.format(n, symbol)
                     for symbol, n in count.items())
    print('E({}) - {} = {:.6f} eV'.format(atoms.get_chemical_formula(),
                                          ref, fe))
