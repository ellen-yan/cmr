from ase.io import read
from ase.io.trajectory import Trajectory
from ase.constraints import UnitCellFilter
from ase.optimize import BFGS
from gpaw import GPAW, FermiDirac, PW

from c2dm.relax import relax_done


def relax2x2(kptdensity=6.0, width=0.05, ecut=800, xc='PBE', fmax=0.01,
             stdev=0.001):
    """Relax atomic positions and 2x2 cell.

    Different magnetic states will be tried: non-magnetic, ferromagnetic,
    antiferromagnetic.

    A gs_2x2.gpw file will be written for the most stable configuration.
    Further results can be found in the corresponding trajetries.
    """
    slab = read('gs.gpw')
    slab = slab.repeat((2, 2, 1))

    params = dict(
        mode=PW(ecut),
        xc=xc,
        basis='dzp',
        # parallel={'band': 1},
        kpts={'density': kptdensity, 'gamma': True},
        occupations=FermiDirac(width=width))

    # single point
    try:
        read('ref_2x2.traj').get_potential_energy()
    except Exception as e:
        slab.calc = GPAW(txt='ref_2x2.txt',
                         **params)
        traj = Trajectory('ref_2x2.traj', 'w', slab)
        slab.calc.attach(traj.write)
        slab.get_potential_energy()
        traj.write()
        traj.close()

    # relax rattled
    slab.rattle(stdev=stdev)

    # non magnetic
    if relax_done('relax-spin-paired_2x2.traj', fmax, slab):
        slab = read('relax-spin-paired_2x2.traj')
        e2 = read('relax-spin-paired_2x2.traj').get_potential_energy()
    else:
        slab.set_initial_magnetic_moments([0.] * len(slab))
        slab.calc = GPAW(spinpol=False,
                         txt='relax-spin-paired_2x2.txt',
                         **params)
        relax = BFGS(UnitCellFilter(slab, mask=[1, 1, 0, 0, 0, 1]),
                     logfile='relax-spin-paired_2x2.log',
                     trajectory='relax-spin-paired_2x2.traj')
        relax.run(fmax=fmax)
        e2 = slab.get_potential_energy()
        slab.calc.write('gs_2x2.gpw')

    # fm
    if relax_done('relax-spin-polarized_2x2.traj', fmax, slab):
        slab = read('relax-spin-polarized_2x2.traj')
        e2fm = read('relax-spin-polarized_2x2.traj').get_potential_energy()
    else:
        slab.set_initial_magnetic_moments([1.] * len(slab))
        slab.calc = GPAW(spinpol=True,
                         txt='relax-spin-polarized_2x2.txt',
                         **params)
        relax = BFGS(UnitCellFilter(slab, mask=[1, 1, 0, 0, 0, 1]),
                     logfile='relax-spin-polarized_2x2.log',
                     trajectory='relax-spin-polarized_2x2.traj')
        relax.run(fmax=fmax)
        e2fm = slab.get_potential_energy()
        if e2fm < e2:
            slab.calc.write('gs_2x2.gpw')

    # afm - currently not done
    # slab = read('gs_2x2.gpw')
    # if relax_done('relax-spin-polarized-afm_2x2.traj', fmax, slab):
    #     slab = read('relax-spin-polarized-afm_2x2.traj')
    #     e2afm = slab.get_potential_energy()
    # else:
    #     m = [1.] * len(slab)
    #     m[::2] = [-1.] * int(len(slab) / 2 + .5)
    #     slab.set_initial_magnetic_moments(m)
    #     slab.calc = GPAW(spinpol=True,
    #                      txt='relax-spin-polarized-afm_2x2.txt',
    #                      **params)
    #     relax = BFGS(UnitCellFilter(slab, mask=[1, 1, 0, 0, 0, 1]),
    #                  logfile='relax-spin-polarized-afm_2x2.log',
    #                  trajectory='relax-spin-polarized-afm_2x2.traj')
    #     relax.run(fmax=fmax)
    #     e2afm = slab.get_potential_energy()
    #     if e2afm < min(e2, e2fm):
    #         slab.calc.write('gs_2x2.gpw')


if __name__ == '__main__':
    from c2dm import run
    run(relax2x2)
