#!/bin/bash

# clean up
rm -rf BN-BN c2dm.db

# prepare and calc
python3 -m c2dm.prepare -p auto -c BN.xyz
cd BN-BN
python3 -m c2dm.calc --ecut 400 --vacuum 4 --skip-long > ../test.log

# collect
cd -
python3 ../collect.py --verbose BN-BN >> test.log
