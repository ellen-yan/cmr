from collections import defaultdict
import json

import numpy as np
from ase.dft.dos import ltidos
from ase.io import jsonio
from ase.parallel import paropen
from ase.units import Ha
from ase.utils import basestring
from ase.parallel import rank
import gpaw.mpi as mpi
from gpaw import GPAW
from gpaw.utilities.dos import raw_orbital_LDOS, raw_spinorbit_orbital_LDOS


def count(zs):
    """Count the number of occurences in a list

    Parameters:
    zs: [z1, z2, ...]-list or ndarray
        list of int's or str's

    Returns:
    out: {z1: n1, ...}
        n1 is the number of occurences of z1

    Examples:
    >>> zs = [1, 8, 8]
    >>> count(zs)
    {8: 2, 1: 1}
    >>> zs = ['H', 'O', 'O']
    >>> count(zs)
    {'O': 2, 'H': 1}
    """
    c = defaultdict(int)
    for z in zs:
        c[z] += 1
    return c


def get_l_a(zs):
    """Defines which atoms and angular momentum to project onto.

    Parameters:
    zs: [z1, z2, ...]-list or array
        list of atomic numbers (zi: int)

    Returns:
    out: {int: str, ...}-dict
        keys are atomic indices and values are a string such as 'spd'
        that determines which angular momentum to project onto or a
        given atom
    """
    # Assuming AB2 prototype for TMDs
    zs = np.asarray(zs)
    index = zs.tolist().index
    l_a = {}
    for z, n in count(zs).items():
        a = index(z)
        if n == 1:
            l_a[a] = 'spd'  # spd on atom in pos A
        elif n == 2:
            l_a[a] = 'sp'  # sp on atoms in pos B

    return l_a


def run_pdos(calc='densk.gpw'):
    pdos(calc, spinorbit=False)
    if not isinstance(calc, basestring):
        pdos(calc, spinorbit=True)
    else:
        ranks = [0]
        comm = mpi.world.new_communicator(ranks)
        if mpi.world.rank in ranks:
            new_calc = GPAW(calc, txt=None, communicator=comm)
            pdos(new_calc, spinorbit=True)


def pdos(calc, spinorbit=True):
    """
    Writes the projected dos to a file pdos.json or pdos_soc.json

    Parameters:
    calc: GPAW calculator object or str
        calculator with a method get_orbital_ldos
    spinorbit: bool
        spin orbit coupling
    """
    if spinorbit:
        assert rank == 0, 'spinorbit pdos only works in serial mode'
    if isinstance(calc, basestring):
        calc = GPAW(calc, txt=None)
    zs = calc.atoms.get_atomic_numbers()
    efermi = calc.get_fermi_level()
    l_a = get_l_a(zs)
    kd = calc.wfs.kd

    if spinorbit:
        ldos = raw_spinorbit_orbital_LDOS
    else:
        ldos = raw_orbital_LDOS

    e = np.linspace(-10, 10, 2000)
    ns = calc.get_number_of_spins()
    pdos_sal = {}
    e_s = {}
    for s in range(ns):
        pdos_sal[s] = {}
        for a in l_a:
            pdos_sal[s][a] = {}
            for l in l_a[a]:
                energies, weights = ldos(calc, a, s, l)
                energies.shape = (kd.nibzkpts, -1)
                energies = energies[kd.bz2ibz_k]
                energies.shape = tuple(kd.N_c) + (-1,)
                weights.shape = (kd.nibzkpts, -1)
                weights /= kd.weight_k[:, np.newaxis]
                w = weights[kd.bz2ibz_k]
                w.shape = tuple(kd.N_c) + (-1,)
                p = ltidos(calc.atoms.cell, energies * Ha, e, w)
                pdos_sal[s][a][l] = p
        e_s[s] = e

    data = {'energies': e,
            'pdos_sal': pdos_sal,
            'symbols': calc.atoms.get_chemical_symbols(),
            'efermi': efermi}
    fname = 'pdos_soc.json' if spinorbit else 'pdos.json'
    with paropen(fname, 'w') as fd:
        json.dump(jsonio.encode(data), fd)


def plot_pdos():
    """only for testing
    """
    efermi = GPAW('gs.gpw', txt=None).get_fermi_level()
    import matplotlib.pyplot as plt
    with paropen('pdos.json', 'r') as fd:
        data = jsonio.decode(json.load(fd))
        e = np.asarray(data['energies'])
        pdos_sal = data['pdos_sal']
        symbols = data['symbols']

    with paropen('evac.txt', 'r') as fd:
        evac = float(fd.read())

    e -= evac
    pmax = 0.0
    for s, pdos_al in pdos_sal.items():
        for a, pdos_l in sorted(pdos_al.items()):
            for l, pdos in sorted(pdos_l.items(), reverse=True):
                pdos = np.asarray(pdos)
                pmax = max(pdos.max(), pmax)
                plt.plot(pdos, e, label='{} ({})'.format(symbols[int(a)], l))
    plt.xlim(0, pmax)
    plt.ylim(efermi - evac - 2, efermi - evac + 2)
    plt.legend()
    plt.ylabel('energy relative to vacuum [eV]')
    plt.xlabel('pdos [states/eV]')
    plt.show()


if __name__ == '__main__':
    from c2dm import run
    run(run_pdos, 'densk.gpw')
