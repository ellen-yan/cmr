from __future__ import print_function
import numpy as np
from ase.units import Bohr


# NFH, FR 04/17 Lyngby DTU
# This function is usable if you want to check whether a k-point is between
# two high-symmetry points:
def is_point_between(x, a, b):
    """Returns True if the point x is between the points a and b."""
    ab = b - a
    ax = x - a
    cosu = np.dot(ab, ax) / np.sqrt(np.sum(ab**2) * np.sum(ax**2))
    if abs(abs(cosu) - 1) < 1e-6:
        d_ax = ax[0] / ab[0]
        if d_ax <= 1 and d_ax >= 0:
            return True
    else:
        return False


def get_kpts_inside_sphere(rcell_cv, N_c, kradius):
    """
    Returns a list of grid vectors inside a sphere in k-space.

    rcell_cv: Reciprocal lattice vectors
    N_c: Number of grid points in each direction
    kradius: Radius of k-space sphere
    """
    # Determine how many grid points we need in each direction:
    r1_c = np.dot(kradius * np.array([1, 0, 0]), np.linalg.inv(rcell_cv))
    r2_c = np.dot(kradius * np.array([0, 1, 0]), np.linalg.inv(rcell_cv))
    r3_c = np.dot(kradius * np.array([0, 0, 1]), np.linalg.inv(rcell_cv))
    rmax = np.amax(np.array([np.absolute(r1_c),
                             np.absolute(r2_c),
                             np.absolute(r3_c)]))
    # print('rmax=%s' % rmax)
    nmax = np.asarray(np.ceil(rmax * N_c), int)
    # print('nmax=%s' % nmax)

    dir1_c = np.array([1., 0., 0.]) / N_c[0]
    dir2_c = np.array([0., 1., 0.]) / N_c[1]
    dir3_c = np.array([0., 0., 1.]) / N_c[2]

    kpts = []
    # Go through grid and add k-point to list if it is within sphere
    for n1 in range(-nmax[0], nmax[0] + 1):
        for n2 in range(-nmax[1], nmax[1] + 1):
            for n3 in range(-nmax[2], nmax[2] + 1):
                kpt_c = n1 * dir1_c + n2 * dir2_c + n3 * dir3_c
                kpt_v = np.dot(kpt_c, rcell_cv)
                if np.linalg.norm(kpt_v) <= kradius + 1e-9:
                    kpts.append(kpt_c)

    return np.array(kpts)


# This function should fit a paraboloid to a set of points in 3d
# least square fit - allows for missing the CBM/VBM slightly by including
# linear terms however getting as close as possible to the CBM/VBM will give
# best results.
# The user should use dense (at least 15/15/15) meshes for the scf-cycle -
# the more high-symmetry-points are included in the mesh the better
def fit_3d_paraboloid(bskpts_kv, eps_k):
    """Fit a 3d paraboloid to a list of values in each of number of
    coordinates"""
    G = np.zeros((len(bskpts_kv), 10))
    for k, k_v in enumerate(bskpts_kv):
        K_v = bskpts_kv[k] * Bohr
        G[k, :] = np.array([K_v[0]**2, K_v[1]**2, K_v[2]**2,
                            K_v[0] * K_v[1], K_v[0] * K_v[2], K_v[1] * K_v[2],
                            K_v[0], K_v[1], K_v[2], 1])

    A = np.linalg.lstsq(G, eps_k)[0]
    # Construct coefficient matrix from lstsq solution:
    M = np.array([[A[0], 1 / 2. * A[3], 1 / 2. * A[4]],
                  [1 / 2. * A[3], A[1], 1 / 2. * A[5]],
                  [1 / 2. * A[4], 1 / 2. * A[5], A[2]]])
    # Diagonalize:
    w, V = np.linalg.eig(M)

    return w, V, A
