import argparse
import json
import os
import warnings

from ase import Atoms
from ase.constraints import UnitCellFilter
from ase.io import read
import ase.io.ulm as ulm
from ase.optimize import BFGS
from ase.parallel import world
from gpaw import GPAW, PW, FermiDirac

from c2dm import readinfo
from c2dm.prepare import convert_to_canonical_structure


def relax_done(fname, fmax=0.01, slab=None, verbose=False):
    """Check if a relaxation is done"""
    # check if file is present and has a strucuture
    try:
        slab2 = read(fname)
    except (FileNotFoundError, OSError, IndexError):
        if verbose:
            print("relax ", fname, ": Not found!")
        return False

    # correct elements
    if slab is not None:
        if slab.get_chemical_formula() != slab2.get_chemical_formula():
            if verbose:
                print("relax ", fname, ": Wrong compound!")
            return False

    # minimization finished
    if (slab2.get_forces()**2).sum(1).max() >= fmax:
        if verbose:
            print("relax ", fname, ": Forces do not vanish!")
        return False

    # some relaxations went wrong
    for i, j in [(0, 2), (1, 2), (2, 0), (2, 1)]:
        if abs(slab2._cell[i, j]) > 1e-12:
            if verbose:
                print("relax ", fname, ": Cell is no longer 2D!")
            return False

    # k-mesh sufficent
    try:
        r = ulm.open(fname)
        dens = r.calculator.parameters.get("kpts").get("density")
        if dens < 6.0:
            if verbose:
                print("relax ", fname, ": k-mesh is too small!")
            return False
    except (FileNotFoundError, OSError):
        pass

    # otherwise ok
    return True


def relax(slab, vacuum=7.5, kptdensity=6.0, width=0.05, ecut=800, xc='PBE',
          fmax=0.01):
    """Relax atomic positions and unit cell.

    Different magnetic states will be tried: non-magnetic, ferro-magnetic, ...

    A gs.gpw file will be written for the most stable configuration.
    """

    slab.pbc = (1, 1, 0)
    slab._cell[2][0:2] = 0.
    slab._cell.T[2][0:2] = 0.
    slab.center(vacuum=vacuum, axis=2)

    # are we already done?
    if (os.path.isfile('gs.gpw') and
        relax_done('relax-spin-paired.traj', fmax, slab) and
        relax_done('relax-spin-polarized.traj', fmax, slab)):
        return read('gs.gpw')

    params = dict(
        mode=PW(ecut),
        xc=xc,
        basis='dzp',
        # parallel={'band': 1},
        kpts={'density': kptdensity, 'gamma': True},
        occupations=FermiDirac(width=width))

    slab.set_initial_magnetic_moments(None)
    slab.calc = GPAW(txt='relax-spin-paired.txt', **params)
    uf = UnitCellFilter(slab, mask=[1, 1, 0, 0, 0, 0])
    relax = BFGS(uf,
                 logfile='relax-spin-paired.log',
                 trajectory='relax-spin-paired.traj')
    relax.run(fmax=fmax)
    slab.calc.write('gs.gpw')
    e1 = slab.get_potential_energy()

    slab.set_initial_magnetic_moments([1] * len(slab))
    slab.calc = GPAW(txt='relax-spin-polarized.txt', **params)
    relax = BFGS(uf,
                 logfile='relax-spin-polarized.log',
                 trajectory='relax-spin-polarized.traj')
    relax.run(fmax=fmax)
    e2 = slab.get_potential_energy()
    m = slab.get_magnetic_moment()

    # assume precison of 10 meV per atom
    de = (e2 - e1) / len(slab)
    mm = abs(m) / len(slab)
    if de < -0.01:
        # magnetic groundstate
        slab.calc.write('gs.gpw')
        if mm <= 0.1:
            warnings.warn('Magnetic, but mm small!')
    elif (de < 0.01 and mm > 0.1):
        # store both
        slab.calc.write('gs-polarized.gpw')
    else:
        # otherwise non-magnetic
        if mm > 0.1:
            warnings.warn('Non-magnetic gs, but also found magnetic!')

    # return gs
    return read('gs.gpw')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=relax.__doc__)
    parser.add_argument('-p', '--prototype', help='One of MoS2, CdI2, ...')
    parser.add_argument('-f', '--fmax', help='fmax', default=0.01)
    parser.add_argument('-c', '--create-folder', action='store_true',
                        help='Create a "Formula-Prototype/" folder and run '
                        'calculation in that folder.')
    parser.add_argument('filename', help='Filename of input structure.')
    args = parser.parse_args()
    atoms = read(args.filename)
    formula = atoms.get_chemical_formula('metal')
    prototype = args.prototype
    if args.create_folder:
        if prototype:
            folder = '{}-{}'.format(formula, prototype)
        else:
            folder = formula
        if world.rank == 0:
            os.mkdir(folder)
        world.barrier()
        os.chdir(folder)
    else:
        path = os.path.basename(os.getcwd())
        f, _, _ = path.partition('-')
        f = Atoms(f).get_chemical_formula('metal')
        if f != formula:
            parser.error('Folder name {} does not start with {}!'
                         .format(path, formula))
        if prototype is None:
            prototype = readinfo().get('prototype')

    if prototype and not os.path.isfile('info.json'):
        with open('info.json', 'w') as fd:
            fd.write(json.dumps({'prototype': prototype}))
    if prototype:
        atoms = convert_to_canonical_structure(atoms, prototype)
    relax(atoms, fmax=args.fmax)
