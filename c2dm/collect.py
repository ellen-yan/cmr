import argparse
import csv
import json
import os
import os.path as op
import traceback

import numpy as np

from ase import Atoms
from ase.db import connect
from ase.dft.bandgap import bandgap
from ase.dft.kpoints import (get_monkhorst_pack_size_and_offset,
                             monkhorst_pack_interpolate, bandpath)
from ase.io import jsonio
from ase.io import read

from gpaw import GPAW

from c2dm import chdir, silence, readinfo
from c2dm.references import read_references, formation_energy
from c2dm.rmsd_monolayer import calculate_rmsd
from c2dm.utils import fermi_level, has_inversion


def gllbsc(kvp, data):
    """PBE +- SOC."""
    if not op.isfile('gllbsc.json'):
        return
    print('Collecting GLLBSC data')
    with open('gllbsc.json') as fd:
        dct = json.load(fd)

    dxc = dct['deltaxc']
    evac = dct['evac']

    with silence():
        calc = GPAW('gllbsc.gpw', txt=None)
    efermi = calc.get_fermi_level() - evac
    ibzkpts = calc.get_ibz_k_points()
    bzkpts = calc.get_bz_k_points()
    nibz = len(ibzkpts)
    eps_kn = np.array([calc.get_eigenvalues(kpt=k) - evac
                       for k in range(nibz)])
    eps_kn[eps_kn > efermi] += dxc

    gap, (k1, n1), (k2, n2) = bandgap(eigenvalues=eps_kn, efermi=efermi,
                                      output=None)
    kvp['gap_gllbsc'] = gap
    if gap:
        vbm = eps_kn[k1, n1]
        cbm = eps_kn[k2, n2]
        dir_gap, p1, p2 = bandgap(eigenvalues=eps_kn, efermi=efermi,
                                  direct=True, output=None)
        kvp.update(dir_gap_gllbsc=dir_gap,
                   vbm_gllbsc=vbm,
                   cbm_gllbsc=cbm)

    path, x, _ = bandpath('GMKG', calc.atoms.cell, 50)
    mpsize, mpoffset = get_monkhorst_pack_size_and_offset(bzkpts)
    bz2ibz = calc.get_bz_to_ibz_map()
    icell = calc.atoms.get_reciprocal_cell()
    eps_kn = monkhorst_pack_interpolate(path, eps_kn,
                                        icell, bz2ibz, mpsize, mpoffset)
    data['bs_gllbsc'] = {'path': path,
                         'efermi': efermi,
                         'eps_skn': eps_kn[np.newaxis]}


def bse(data):
    if not op.isfile('bse_abs_par.csv'):
        return
    print('Collecting BSE data')
    with open('bse_abs_par.csv') as fd:
        par = np.array([[float(x) for x in row]
                        for row in csv.reader(fd)])
    with open('bse_abs_perp.csv') as fd:
        per = np.array([[float(x) for x in row]
                        for row in csv.reader(fd)])
    assert abs(par[0] - per[0]).max() == 0.0
    data['bse_abs'] = {'freq': par[:, 0],
                       'par': par[:, 1],
                       'per': per[:, 1]}


def fermi(data):
    if not op.isfile('fermi_surface.npz'):
        return
    print('Collecting fermi surface data')
    npz = np.load('fermi_surface.npz')
    data['fermisurface'] = npz['contours']


def absorptionspectrum(data):
    if not op.isfile('polarizability_tetra.npz'):
        return
    print('Collecting absorption data')
    dct = dict(np.load('polarizability_tetra.npz'))
    data['absorptionspectrum'] = dct


def bs(nspins, kvp, data):
    """Band structure PBE and GW +- SOC."""
    if not op.isfile('eigs_spinorbit.npz'):
        return
    print('Collecting bands-structure data')
    nosoc = GPAW('bs.gpw', txt=None)
    with open('eigs_spinorbit.npz', 'rb') as fd:
        soc = dict(np.load(fd))
    path = nosoc.get_bz_k_points()
    npoints = len(path)
    eps_skn = np.array([[nosoc.get_eigenvalues(kpt=k, spin=s)
                         for k in range(npoints)]
                        for s in range(nspins)])
    with open('evac.txt') as fd:
        evac = float(fd.read())

    s_mvk = soc.get('s_mvk', soc.get('s_mk'))
    if s_mvk.ndim == 3:
        sz_mk = s_mvk[:, 2, :]  # take z component
        if sz_mk.shape.index(npoints) == 0:
            sz_mk = sz_mk.transpose()
    else:
        sz_mk = s_mvk

    assert sz_mk.shape[1] == npoints, 'sz_mk has wrong dims'

    pbe = {
        'path': path,
        'eps_skn': eps_skn - evac,
        'efermi': nosoc.get_fermi_level() - evac,
        'eps_so_mk': soc['e_mk'] - evac,
        'sz_mk': sz_mk}
    if nspins == 1 and has_inversion(nosoc.get_atoms()):
        # non-magnetic and has inversion symmetry:
        pbe['sz_mk'][:] = 0.0
    data['bs_pbe'] = pbe

    if os.path.isfile('gw.npz'):
        dct = np.load('gw.npz')
        data['bs_gw'] = {
            'path': dct['path'],
            'eps_skn': dct['eps_skn'] - evac,
            'efermi': dct['efermi'] - evac}
        for key in ['gap_gw', 'dir_gap_gw']:
            kvp[key] = float(dct[key])
        for key in ['cbm_gw', 'vbm_gw']:
            kvp[key] = float(dct[key]) - evac


def hse(kvp, data):
    if not op.isfile('hse_bandstructure.npz'):
        return
    with open('evac.txt') as fd:
        evac = float(fd.read())
    dct = dict(np.load('hse_bandstructure.npz'))
    calc = GPAW('hse_nowfs.gpw', txt=None)
    ibzkpts = calc.get_ibz_k_points()
    # without soc first
    eps_skn = np.load('hse_eigenvalues.npz')['e_hse_skn']
    efermi_nosoc = fermi_level(calc, eps_skn=eps_skn)
    data['bs_hse'] = {
        'path': dct['path'],
        'eps_skn': dct['eps_skn'] - evac,
        # 'kptpath': str(dct['kptpath']),
        'efermi_nosoc': efermi_nosoc - evac}

    gap, p1, p2 = bandgap(eigenvalues=eps_skn, efermi=efermi_nosoc,
                          output=None)
    gapd, p1d, p2d = bandgap(eigenvalues=eps_skn, efermi=efermi_nosoc,
                             direct=True, output=None)
    if gap:
        data['bs_hse']['kvbm_nosoc'] = ibzkpts[p1[1]]
        data['bs_hse']['kcbm_nosoc'] = ibzkpts[p2[1]]
        vbm = eps_skn[p1] - evac
        cbm = eps_skn[p2] - evac
        kvp.update(vbm_hse_nosoc=vbm, cbm_hse_nosoc=cbm,
                   dir_gap_hse_nosoc=gapd, gap_hse_nosoc=gap)
    # then with soc if available
    if 'e_mk' in dct and op.isfile('hse_eigenvalues_soc.npz'):
        e_mk, s_mk = dct['e_mk'], dct['s_mk']  # band structure
        eps = np.load('hse_eigenvalues_soc.npz')['e_hse_mk']
        eps = eps.transpose()[np.newaxis]
        efermi = fermi_level(calc, eps_skn=eps,
                             nelectrons=calc.get_number_of_electrons() * 2)
        data['bs_hse'].update(eps_mk=e_mk - evac,
                              sz_mk=s_mk,
                              efermi=efermi - evac)
        if len(eps_skn) == 1 and has_inversion(calc.get_atoms()):
            # non-magnetic and has inversion symmetry:
            data['bs_hse']['sz_mk'][:] = 0.0
        gap, p1, p2 = bandgap(eigenvalues=e_mk.transpose(), efermi=efermi,
                              output=None)
        gapd, p1d, p2d = bandgap(eigenvalues=e_mk.transpose(), efermi=efermi,
                                 direct=True, output=None)
        if gap:
            data['bs_hse']['kvbm'] = ibzkpts[p1[1]]
            data['bs_hse']['kcbm'] = ibzkpts[p2[1]]
            vbm = e_mk.transpose()[p1] - evac
            cbm = e_mk.transpose()[p2] - evac
            kvp.update(vbm_hse=vbm, cbm_hse=cbm,
                       dir_gap_hse=gapd, gap_hse=gap)


def anisotropy(kvp, data):
    if not op.isfile('anisotropy.npz'):
        return
    dct = np.load('anisotropy.npz')
    kvp['maganis'] = dct['dE'] * 1e3


def ehmasses(kvp, data):
    for x, y in zip(('_soc', ''), ('', '_nosoc')):
        name = 'masstensor{}.npz'.format(x)
        if op.isfile(name):
            print('Collecting charge carrier effective mass data')
            d = np.load(name)
            mt = d['masstensor_cbm']
            kvp['emass1' + y], kvp['emass2' + y] = np.sort(mt[:2])
            mt = d['masstensor_vbm']
            kvp['hmass1' + y], kvp['hmass2' + y] = -np.sort(mt[:2])


def stiffness_tensor(kvp, data):
    print('Collecting stiffness tensor and speed of sound.')
    if os.path.isfile('strain_quantities.npz'):
        d = np.load('strain_quantities.npz')
        stiffness = d['stiffness_tensor']
        speed_of_sound = d['speed_of_sound']
        relative_difference = np.abs(((stiffness[0, 0] - stiffness[1, 1]) /
                                      stiffness[0, 0]))
        if relative_difference < 0.1:  # less than 10% difference between x & y
            kvp['bulk_modulus'] = (stiffness[0, 0] + stiffness[1, 1]) / 2
            kvp['speed_of_sound'] = np.mean(speed_of_sound)
        else:
            kvp['c_11'] = stiffness[0, 0]
            kvp['c_22'] = stiffness[1, 1]
            kvp['speed_of_sound_x'] = speed_of_sound[0]
            kvp['speed_of_sound_y'] = speed_of_sound[1]


def deformation_potential(kvp, data):
    print('Collecting deformation potentials and mobilities.')
    name = 'strain_quantities.npz'
    if os.path.isfile(name):
        d = np.load(name)
        try:
            D_nosoc = d['deformation_potentials_nosoc']
            D = d['deformation_potentials']
            data['deformation_potentials'] = D
            kvp['D_vbm'] = D[2, 0]
            kvp['D_cbm'] = D[2, 1]
            kvp['D_vbm_nosoc'] = D_nosoc[2, 0]
            kvp['D_cbm_nosoc'] = D_nosoc[2, 1]
            data['deformation_potentials'] = D
            # mobilities = d['mobilities']
            # mobilities_nosoc = d['mobilities_nosoc']
            # kvp['mobility_x'] = mobilities[0]
            # kvp['mobility_y'] = mobilities[1]
            # kvp['mobility_x_nosoc'] = mobilities_nosoc[0]
            # kvp['mobility_y_nosoc'] = mobilities_nosoc[1]
        except KeyError:
            return


def colgap(kvp, data, efermi):
    if not op.isfile('evac.txt'):
        return
    with open('evac.txt') as fd:
        evac = float(fd.read())
    for x, y in zip(('_soc', ''), ('', '_nosoc')):
        name = 'gap{}.npz'.format(x)
        if op.isfile(name):
            d = np.load(name)
            if d['vbm'].tolist() is None:  # it's a metal
                print('Collecting PBE{} work-function'.format(y))
                # kvp['gap' + y] = 0.0
                kvp['is_metallic' + y] = True
                kvp['work_function' + y] = evac - efermi
            else:
                print('Collecting PBE{} gap, vbm, cbm, ... data'.format(y))
                kvp['is_metallic' + y] = False
                kvp['gap' + y] = float(d['gap'])
                kvp['dir_gap' + y] = float(d['gap_dir'])
                kvp['dir_gap' + y] = float(d['gap_dir'])
                kvp['cbm' + y] = float(d['cbm']) - evac
                kvp['vbm' + y] = float(d['vbm']) - evac
                kvp['dir_vbm' + y] = float(d['vbm_dir']) - evac
                kvp['dir_cbm' + y] = float(d['cbm_dir']) - evac
                if data.get('bs_pbe') is None:
                    data['bs_pbe'] = {}
                data['bs_pbe']['kvbm' + y] = d['k1_c']
                data['bs_pbe']['kcbm' + y] = d['k2_c']
                kvp['is_dir_gap' + y] = kvp['dir_gap' + y] == kvp['gap' + y]


def pdos(data):
    def getit(fname):
        with open('evac.txt') as fd:
            evac = float(fd.read())
        with open(fname) as fd:
            dct = jsonio.decode(json.load(fd))
            pdos_sal = dct['pdos_sal']
            e = dct['energies'] - evac
            efermi = dct['efermi'] - evac
        return {'pdos_sal': pdos_sal, 'energies': e, 'efermi': efermi}

    if op.isfile('pdos.json'):
        print('Collecting pdos w/o soc')
        data['pdos_pbe_nosoc'] = getit('pdos.json')
    if op.isfile('pdos_soc.json'):
        print('Collecting pdos w soc')
        data['pdos_pbe'] = getit('pdos_soc.json')


def collect(formula, info, db, references, verbose=False):
    candidates = []
    for fname in ['relax-spin-paired.traj', 'relax-spin-polarized.traj']:
        atoms = read(fname)
        assert formula == atoms.get_chemical_formula()
        e = atoms.get_potential_energy() / len(atoms)
        f = atoms.get_forces()
        s = atoms.get_stress()[:2]
        fmax = (f**2).sum(1).max()
        smax = abs(s).max() * atoms.get_volume() / len(atoms)
        assert fmax < 0.01, fmax
        assert smax < 0.01, smax
        candidates.append(e)

    calc = GPAW('gs.gpw', txt=None)
    atoms = calc.get_atoms()

    kvp = {}
    data = {}

    try:
        import spglib
    except ImportError:
        pass
    else:
        sg, number = spglib.get_spacegroup(atoms, symprec=1e-4).split()
        number = int(number[1:-1])
        print('Spacegroup:', sg, number)
        kvp['spacegroup'] = sg

    prototype = info.get('prototype')
    cls = info.get('class')
    if prototype:
        kvp['prototype'] = prototype
    if cls:
        kvp['class'] = cls
    nspins = int(calc.get_spin_polarized()) + 1
    kvp['is_magnetic'] = magnetic = (nspins == 2)
    assert magnetic == (candidates[1] < candidates[0] - 0.01)
    if not magnetic:
        atoms.calc.results['magmom'] = 0.0
    if verbose:
        print('Magnetic:', magnetic)

    kvp['hform'] = formation_energy(atoms, references) / len(atoms)
    if verbose:
        print('Heat form:', kvp['hform'])

    # stability (negative heat, 2x2 less then 10 meV deeper in energy)
    if op.isfile('ref_2x2.traj'):
        e1 = read('ref_2x2.traj').get_potential_energy()
        e2_list = [
            read('relax-spin-paired_2x2.traj').get_potential_energy(),
            read('relax-spin-polarized_2x2.traj').get_potential_energy(),
            read('relax-spin-polarized-afm_2x2.traj').get_potential_energy()]
        e2 = min(e2_list)
        if e2_list.index(e2) == 0:
            s2 = read('relax-spin-paired_2x2.traj')
            if verbose:
                print('2x2: non-magnetic')
        elif e2_list.index(e2) == 1:
            s2 = read('relax-spin-polarized_2x2.traj')
            if verbose:
                print('2x2: ferromagnetic')
        else:
            s2 = read('relax-spin-polarized-afm_2x2.traj')
            if verbose:
                print('2x2: antiferromagnetic')
        d2 = float(calculate_rmsd(read('ref_2x2.traj'), s2)[0])
        if verbose:
            print('2x2: rmsd =', d2)

        kvp['is_dyn_stable'] = (((e2 - e1) / len(s2) < 10e-3) and (d2 < 0.05))
        if verbose:
            print('Dyn stab: {} ({})'.format(kvp['is_dyn_stable'],
                                             (e2 - e1) / len(s2)))

        if kvp['hform'] < 0 and kvp['is_dyn_stable']:
            kvp['stability'] = 'maybe'
        else:
            kvp['stability'] = 'unstable'

    efermi = calc.get_fermi_level()

    bse(data)
    ehmasses(kvp, data)
    gllbsc(kvp, data)
    bs(nspins, kvp, data)
    hse(kvp, data)
    anisotropy(kvp, data)
    colgap(kvp, data, efermi)
    fermi(data)
    pdos(data)
    absorptionspectrum(data)
    stiffness_tensor(kvp, data)
    deformation_potential(kvp, data)
    if db is not None:
        db.write(atoms, data=data, **kvp)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Collect data for c2dm.db')
    parser.add_argument('folder', nargs='+')
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('-n', '--dry-run', action='store_true')
    args = parser.parse_args()

    # We use absolute path because of chdir below!
    dbname = os.path.join(os.getcwd(), 'c2dm.db')
    if not args.dry_run:
        db = connect(dbname)

    references = read_references()

    errors = []
    for folder in args.folder:
        if not os.path.isdir(folder):
            continue
        with chdir(folder):
            print(folder, end=': ')
            folder = os.path.basename(os.getcwd())
            formula, _, _ = folder.partition('-')
            formula = Atoms(formula).get_chemical_formula()  # normalize
            print(formula, end='')
            info = readinfo()
            prototype = info.get('prototype')
            if prototype:
                print(' [' + prototype + ']')
            else:
                print()
            try:
                collect(formula, info, db, references,
                        verbose=args.verbose)
            except KeyboardInterrupt:
                break
            except Exception as x:
                error = '{}: {}'.format(x.__class__.__name__, x)
                tb = traceback.format_exc()
                print(error)
                errors.append((folder, error, tb))

    if errors:
        print('Errors:')
        for error in errors:
            print('{}\n{}: {}\n{}'.format('=' * 77, *error))
