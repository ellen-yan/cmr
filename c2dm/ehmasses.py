"""
These methods are used to find band edges and effective mass tensors
for semiconductors and fermi surfaces for metals
                 -------
                |  gap()|
-------------   | metal?|      -----------------
| nonsc():   |  --------  ->   | fermisurface() |
| > densk.gpw|   No |      yes  -----------------
--------------      |
                    v
               ----------------------
               | nonsc_sphere(soc):   |
               | > sphere_cbm.gpw     |
               | > sphere_vbm.gpw     |
               | > sphere_cbm_soc.gpw |
               | > sphere_vbm_soc.gpw |
               | bg():                |
               | > gap.npz            |
               | > gap_soc.npz        |
               |   - vbm, cbm, ...    |
               | masstensor(soc):     |
               |  > masstensor.npz    |
               |  > masstensor_soc.npz|
               | -effective masses    |
               -----------------------


Note with SOC then symmetry should be off for magnetic materials,
but one can use the full symmetry for non magnetic materials for the
eigenvalues (the spin projection and wave-functions will be wrong though)
"""

import os.path as op
import numpy as np
from math import pi
from ase.dft.bandgap import bandgap
import gpaw.mpi as mpi
from ase.parallel import paropen, broadcast
from ase.units import Hartree
from gpaw import GPAW
from gpaw.spinorbit import get_spinorbit_eigenvalues, set_calculator
from c2dm.gap import get_kpts_inside_sphere, fit_3d_paraboloid
from gpaw.kpt_descriptor import to1bz


def socstr(soc):
    return '_soc' if soc else ''


def runeh(kptdensdens=12.0):
    nonsc(kdens=kptdensdens)
    bg()
    for soc in [True, False]:
        e1e2g, skn1, skn2 = gap(soc=soc, direct=False)
        if e1e2g[2] < 0.01:
            pass
        else:
            nonsc_spheres(soc=soc)

    for soc in [True, False]:
        if op.isfile('sphere_vbm' + socstr(soc) + '.gpw'):
            masstensor(soc)


def bg():
    """find bandgap, vbm, cbm, k_cbm, k_vbm in densk.gpw and save it to
    it to gap.npz and gap_soc.npz
    """
    if not op.isfile('densk.gpw'):
        return

    calc = GPAW('densk.gpw', txt=None)
    ibzkpts = calc.get_ibz_k_points()
    efermi = calc.get_fermi_level()
    for soc in [True, False]:
        e1e2g, skn1, skn2 = gap(soc=soc, direct=False)
        e1e2g_dir, skn1_dir, skn2_dir = gap(soc=soc, direct=True)
        k1, k2 = skn1[1], skn2[1]
        k1_dir, k2_dir = skn1_dir[1], skn2_dir[1]
        k1_c = ibzkpts[k1] if k1 is not None else None
        k2_c = ibzkpts[k2] if k2 is not None else None
        if k1_c is not None:
            k1_c = to1bz([k1_c], calc.wfs.gd.cell_cv)[0]
        if k2_c is not None:
            k2_c = to1bz([k2_c], calc.wfs.gd.cell_cv)[0]
        k1_dir_c = ibzkpts[k1_dir] if k1_dir is not None else None
        k2_dir_c = ibzkpts[k2_dir] if k2_dir is not None else None
        if k1_dir_c is not None:
            k1_dir_c = to1bz([k1_dir_c], calc.wfs.gd.cell_cv)[0]
        if k2_dir_c is not None:
            k2_dir_c = to1bz([k2_dir_c], calc.wfs.gd.cell_cv)[0]
        data = {'gap': e1e2g[2],
                'vbm': e1e2g[0],
                'cbm': e1e2g[1],
                'gap_dir': e1e2g_dir[2],
                'vbm_dir': e1e2g_dir[0],
                'cbm_dir': e1e2g_dir[1],
                'k1_c': k1_c,
                'k2_c': k2_c,
                'k1_dir_c': k1_dir_c,
                'k2_dir_c': k2_dir_c,
                'skn1': skn1,
                'skn1_dir': skn1_dir,
                'skn2': skn2,
                'skn2_dir': skn2_dir,
                'efermi': efermi}
        with paropen('gap{}.npz'.format(socstr(soc)), 'wb') as f:
            np.savez(f, **data)


def nonsc(kdens=12, emptybands=20, outname='densk'):
    """Non self-consistent calculation with dense k-point sampling
       based on the density in gs.gpw
    """
    if op.isfile(outname + '.gpw'):
        return GPAW(outname + '.gpw', txt=None)

    calc = GPAW('gs.gpw', txt=None)
    spinpol = calc.get_spin_polarized()

    def get_kpts_size(atoms, density):
        """trying to get a reasonable monkhorst size which hits high
        symmetry points
        """
        from gpaw.kpt_descriptor import kpts2sizeandoffsets as k2so
        size, offset = k2so(atoms=calc.atoms, density=density)
        size[2] = 1
        for i in range(2):
            if size[i] % 6 != 0:
                size[i] = 6 * (size[i] // 6 + 1)

        kpts = {'size': size, 'gamma': True}
        return kpts

    kpts = get_kpts_size(atoms=calc.atoms, density=kdens)
    convbands = int(emptybands / 2)
    calc.set(nbands=-emptybands,
             basis='dzp',
             txt=outname + '.txt',
             fixdensity=True,
             kpts=kpts,
             convergence={'bands': -convbands})

    if spinpol:
        calc.set(symmetry='off')  # due to soc

    calc.get_potential_energy()
    calc.write(outname + '.gpw')
    return calc


def gap(soc, direct=False, calc=None):
    """Extension of ase.dft.bandgap.bandgap to also work with spin orbit
    coupling and return vbm (e1) and cbm (e2)
    returns (e1, e2, gap), (s1, k1, n1), (s2, k2, n2)
    """
    if soc:
        x = ([None, None, 0.0], [0, None, None], [0, None, None])
        ranks = [0]
        comm = mpi.world.new_communicator(ranks)
        if mpi.world.rank in ranks:
            calc = calc or GPAW('densk.gpw', txt=None, communicator=comm)
            e_mk = get_spinorbit_eigenvalues(calc)
            set_calculator(calc, e_mk.T)
            x1 = bandgap(calc, direct=direct, output=None)
            g, skn1, skn2 = x1
            if skn1[1] is not None:
                e1 = e_mk[skn1[2], skn1[1]]
                e2 = e_mk[skn2[2], skn2[1]]
                x[0][:2] = (e1, e2)
            x[0][2] = g
            x[1][:], x[2][:] = x1[1], x1[2]
        x = broadcast(x, root=0, comm=mpi.world)
        x = (tuple(x[0]), tuple(x[1]), tuple(x[2]))
    else:
        calc = calc or GPAW('densk.gpw', txt=None)
        g, skn1, skn2 = bandgap(calc, direct=direct, output=None)
        if skn1[1] is not None:
            e1 = calc.get_eigenvalues(spin=skn1[0], kpt=skn1[1])[skn1[2]]
            e2 = calc.get_eigenvalues(spin=skn2[0], kpt=skn2[1])[skn2[2]]
        else:
            e1, e2 = None, None
        x = ((e1, e2, g), skn1, skn2)
    return x


def nonsc_spheres(kpts=(100, 100, 1), soc=False, emptybands=20):
    """sample around vbm and cbm in k-space
    """
    convbands = int(emptybands / 2)
    e1e2g, skn1, skn2 = gap(soc)
    e1, e2, g = e1e2g
    calc = GPAW('densk.gpw', txt=None)

    def get_kpts_xk(calc, kpts, kr=0.015):
        ibzkpts = calc.get_ibz_k_points()
        rcell_cv = calc.atoms.get_reciprocal_cell() * 2 * pi
        krad = kr * np.linalg.norm(rcell_cv[0])  # XXX consider rcell_cv[1]?
        kgrid = np.asarray(kpts, int)
        k2add_kc = get_kpts_inside_sphere(rcell_cv, kgrid, krad)
        k1_c, k2_c = ibzkpts[[skn1[1], skn2[1]]]
        k1_kc = k1_c + k2add_kc
        k2_kc = k2_c + k2add_kc
        return k1_kc, k2_kc

    k1_kc, k2_kc = get_kpts_xk(calc, kpts)
    name = 'sphere_cbm' + socstr(soc)
    if not op.isfile(name + '.gpw'):  # XXX for testing
        calc = GPAW('gs.gpw',
                    nbands=-emptybands,
                    fixdensity=True,
                    basis='dzp',
                    kpts=k2_kc,
                    symmetry='off',
                    txt=name + '.txt',
                    convergence={'bands': -convbands})
        calc.get_potential_energy()
        calc.write(name + '.gpw')

    name = 'sphere_vbm' + socstr(soc)
    if not op.isfile(name + '.gpw'):  # XXX for testing
        calc = GPAW('gs.gpw',
                    nbands=-20,
                    fixdensity=True,
                    basis='dzp',
                    kpts=k1_kc,
                    symmetry='off',
                    txt=name + '.txt',
                    convergence={'bands': -convbands})
        calc.get_potential_energy()
        calc.write(name + '.gpw')


def masstensor(soc):
    """
    calculate masstensor from calculations with sampling around
    band max/min k kpts
    """
    # XXX mpi stuff is due to soc is not working in parallel
    def get_eigenvalues(calc):
        ibzkpts = calc.get_ibz_k_points()
        nk = len(ibzkpts)
        ns = calc.get_number_of_spins()
        e_skn = [[calc.get_eigenvalues(spin=s, kpt=k) for k in range(nk)] for s
                 in range(ns)]

        return np.asarray(e_skn)

    e1e2g, skn1, skn2 = gap(soc)
    ranks = [0]
    comm = mpi.world.new_communicator(ranks)
    d = {}
    if mpi.world.rank in ranks:
        for (s, k, n), x in zip((skn1, skn2), ('vbm', 'cbm')):
            calc = GPAW('sphere_' + x + socstr(soc) + '.gpw',
                        communicator=comm, txt=None)
            if soc:
                e_mk = get_spinorbit_eigenvalues(calc)
                b_k = e_mk[n] / Hartree
            else:
                e_skn = get_eigenvalues(calc)
                b_k = e_skn[s, :, n] / Hartree

            kpts_kc = calc.get_ibz_k_points()
            rcell_cv = calc.atoms.get_reciprocal_cell() * 2 * pi
            kpts_kv = np.dot(kpts_kc, rcell_cv)
            w, V, A = fit_3d_paraboloid(kpts_kv, b_k)
            # w: slopes along x,y,z
            d['masstensor_' + x] = 0.5 / w
            d['eigenvector_' + x] = V
        with paropen('masstensor' + socstr(soc) + '.npz', 'wb') as f:
            np.savez(f, **d)
    d = broadcast(d, root=0, comm=mpi.world)
    return d


if __name__ == '__main__':
    from c2dm import run
    run(runeh)
