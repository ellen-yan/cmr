import argparse
from c2dm import chdir
import os
import json


def get_prototype(name):
    return (name.split('-')[1]).split('/')[0]


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Create info.json files')
    parser.add_argument('folder', nargs='+')
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('-c', '--cls', default='')
    args = parser.parse_args()
    d = {'prototype': '', 'class': args.cls}
    folders = args.folder
    for folder in folders:
        if not os.path.isdir(folder):
            continue
        prototype = get_prototype(folder)
        with chdir(folder):
            d['prototype'] = prototype
            with open('info.json', 'w') as fd:
                json.dump(d, fd, indent=True)
