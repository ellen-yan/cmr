import os.path as op

import numpy as np
from gpaw import GPAW
import gpaw.mpi as mpi
from gpaw.spinorbit import get_anisotropy
from ase.parallel import paropen, parprint


def anisotropy(kptpath=None, width=0.001, nbands=None):
    """Calculate the magnetic anisotropy from densk.gpw."""

    if not op.isfile('densk.gpw'):
        return

    ranks = [0]
    comm = mpi.world.new_communicator(ranks)
    if mpi.world.rank in ranks:
        calc = GPAW('densk.gpw', communicator=comm, txt=None)
        if calc.wfs.nspins == 1:
            parprint("The ground state is nonmagnetic. "
                     "Skipping anisotropy calculation")
            return
        E_x = get_anisotropy(calc, theta=np.pi / 2, nbands=nbands, width=width)
        calc = GPAW('densk.gpw', communicator=comm, txt=None)
        E_z = get_anisotropy(calc, theta=0.0, nbands=nbands, width=width)
        dE = E_z - E_x
        with paropen('anisotropy.npz', 'wb') as f:
            np.savez(f, dE=dE)


if __name__ == '__main__':
    from c2dm import run
    run(anisotropy)
