from math import pi, sin, cos

import numpy as np
import matplotlib.pyplot as plt
from ase.dft.kpoints import (get_special_points, special_paths,
                             parse_path_string, labels_from_kpts)


def bz_vertices(cell):
    from scipy.spatial import Voronoi
    icell = np.linalg.inv(cell)
    I = np.indices((3, 3, 3)).reshape((3, 27)) - 1
    G = np.dot(icell, I).T
    vor = Voronoi(G)
    bz1 = []
    for vertices, points in zip(vor.ridge_vertices, vor.ridge_points):
        if -1 not in vertices and 13 in points:
            normal = G[points].sum(0)
            normal /= (normal**2).sum()**0.5
            bz1.append((vor.vertices[vertices], normal))
    return bz1


def plot(cell, paths, dots, elev=None, scale=1, twod=False):
    """
    cell: 3x3 array
    paths: [(names, points), ...]
        names = ['G', 'K', ..]
        points = [(0, 0, 0), (1/3, 1/3, 0), ...]
        plotted as lines
    dots: [(str, point, scatterplotstyle), ...]
          [('VBM', (1/3, 1/3, 0.4), {'c':'r', 'marker': 's'}), ...]
    twod: boolean
        2d bz
    """
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    Axes3D  # silence pyflakes
    if twod:
        cell = np.asarray(cell).copy()
        cell[2, 2] = 1000
    # plot bz
    fig = plt.figure(figsize=(5, 5))
    ax = fig.gca(projection='3d')

    azim = 0.0
    elev = elev or pi / 2
    x = sin(azim)
    y = cos(azim)
    view = [x * cos(elev), y * cos(elev), sin(elev)]

    bz1 = bz_vertices(cell)

    for points, normal in bz1:
        if np.dot(normal, view) < 0:
            ls = ':'
        else:
            ls = '-'
        x, y, z = np.concatenate([points, points[:1]]).T
        ax.plot(x, y, z, c='k', ls=ls, lw=2)
    # plot paths
    txt = ''
    for names, points in paths:
        x, y, z = np.array(points).T
        ax.plot(x, y, z, c='g', ls='--', lw=2)

        for name, point in zip(names, points):
            x, y, z = point
            if name == 'G':
                name = '\\Gamma'
            elif len(name) > 1:
                name = name[0] + '_' + name[1]
            if len(name) > 0:
                ax.text(x, y, z, '$' + name + '$',
                        ha='left', va='bottom', color='black', size=20)
            txt += '`' + name + '`-'

        txt = txt[:-1] + '|'

    # print(txt[:-1])
    # plot dots
    for name, point, kwargs in dots:
        x, y, z = point
        kwargs['label'] = name  # name of label
        kwargs['s'] = 100  # size of scatter point
        ax.scatter(x, y, z, **kwargs)
    ax.legend(loc='upper center')
    ax.set_axis_off()
    ax.autoscale_view(tight=True)
    s = np.array(paths[0][1]).max() / 0.5 * 0.45 * scale
    ax.set_xlim(-s, s)
    ax.set_ylim(-s, s)
    ax.set_zlim(-s, s)
    ax.set_aspect('equal')
    ax.view_init(azim=azim / pi * 180, elev=elev / pi * 180)


def get_paths(row, twod=True):
    """
    bz paths containing names of bz symmetry-points and points
    """
    cell = row.cell
    icell = np.linalg.inv(cell)
    special_points = get_special_points(cell)
    kpts = row.data.bs_pbe.path  # 'GKMG, GM'
    x, X, names = labels_from_kpts(kpts, row.cell)
    namess = [names]  # e.g. [['G','M','K','G'], ['G','M']]
    paths = []
    for names in namess:
        if twod:
            names = [name for name in names
                     if abs(special_points[name][2]) < 1.0e-10]
        points = []
        for name in names:
            k_v = special_points[name]
            k_c = np.dot(icell, k_v)
            points.append(k_c)
        paths.append((names, points))

    return paths


def plot_fermi(row):
    """
    bz paths containing names of bz symmetry-points and points
    """
    if 'fermisurface' not in row.data:
        return
    vertices = row.data.get('fermisurface')
    ax = plt.gca()
    for i, vertex in enumerate(vertices):
        if vertex[2] == 1:  # Means we are starting new contour
            verts = [vertex]
        elif vertex[2] == 2:  # Continuing contour
            verts.append(vertex)

        # Fermi surface without SOC
        if i + 1 == len(vertices) or vertices[i + 1, 2] == 1:
            verts = np.array(verts)
            verts[:, :2] /= (2 * np.pi)
            if row['is_magnetic']:
                if np.abs(verts[0, 3]) < 1e-8:
                    ax.plot(verts[:, 0], verts[:, 1], zs=0, zdir='z',
                            color='y')
                else:
                    ax.plot(verts[:, 0], verts[:, 1], zs=0, zdir='z',
                            color='b')
            else:
                ax.plot(verts[:, 0], verts[:, 1], zs=0, zdir='z',
                        color='r')
    ax.annotate('Fermi surface without SOC', xy=(0.5, 1), ha='center',
                va='top', xycoords='axes fraction')


def get_dots(row, soc):
    """
    Dots to be plotted in the BZ.

    Parameters:

    row: row object
        ase db row object

    soc: bool
        is spinorbit coupling included

    Returns:

    output: [(str, (3,) ndarray, dict), ] list
        list of on the form [(plot_label, kpt_v, plot_style), ]
    """
    cell = row.cell
    icell = np.linalg.inv(cell)
    bs = row.data.get('bs_pbe')
    if soc:
        kvbm = bs.get('kvbm')
        kcbm = bs.get('kcbm')
    else:
        kvbm = bs.get('kvbm_nosoc')
        kcbm = bs.get('kcbm_nosoc')
    dots = []
    if kvbm is not None:
        style = {'c': 'r', 'alpha': 0.5, 'marker': None}
        dots.append(('VBM', np.dot(icell, kvbm), style))
    if kcbm is not None:
        style = {'c': 'b', 'alpha': 0.5, 'marker': None}
        dots.append(('CBM', np.dot(icell, kcbm), style))

    return dots


def plot_bz(row, soc, fname):
    paths = get_paths(row)
    dots = get_dots(row, soc=soc)
    plot(row.cell, paths, dots, twod=True)
    plot_fermi(row)
    plt.savefig(fname)


if __name__ == '__main__':
    from ase.db import connect
    con1 = connect('c2dm.db')
    row = con1.get(formula='MoS2')
    plot_bz(row)
    quit()
    for X, cell in [
        ('cubic', np.eye(3)),
        # ('fcc', [[0, 1, 1], [1, 0, 1], [1, 1, 0]]),
        # ('bcc', [[-1, 1, 1], [1, -1, 1], [1, 1, -1]]),
        # ('tetragonal', [[1, 0, 0], [0, 1, 0], [0, 0, 1.3]]),
        # ('monoclinic', [[1, 0, 0], [0, 1, 0], [0, 0.2, 1]]),
        ('orthorhombic', [[1, 0, 0], [0, 1.2, 0], [0, 0, 1.4]]),
        ('hexagonal', [[1, 0, 0], [-0.5, 3**0.5 / 2, 0], [0, 0, 1]])]:
        twod = True
        icell = np.linalg.inv(cell)
        special_points = get_special_points(X, cell)
        paths = []
        for names in parse_path_string(special_paths[X]):
            points = []
            if twod:  # only use special points in the 2D BZ
                names = [name for name in names
                         if special_points[name][2] < 1.0e-10]
            for name in names:
                sp = special_points[name]
                points.append(np.dot(icell, special_points[name]))
            paths.append((names, points))

        k_vbmax = [0.333, 0.333, 0]
        k_cbmin = [0.167, 0.083, 0]
        dots = []
        dots.append(('VBM-PBE', np.dot(icell, k_vbmax),
                     {'c': 'r', 'marker': 's', 'alpha': 0.5}))
        dots.append(('CBM-PBE', np.dot(icell, k_cbmin),
                     {'c': 'b', 'marker': None, 'alpha': 0.5}))

        if X == 'bcc':
            scale = 0.6
            elev = pi / 13
        else:
            scale = 1
            elev = None

        plot(cell, paths, dots, elev, scale, twod=twod)
        plt.savefig(X + '.svg')
