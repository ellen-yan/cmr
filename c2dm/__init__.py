import argparse
import json
import os
import sys
import errno
from contextlib import contextmanager

from ase.parallel import parprint
from ase.utils import devnull


def ensure_dir(folder):
    try:
        os.makedirs(folder)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise


def readinfo():
    if os.path.isfile('info.json'):
        with open('info.json') as fd:
            return json.load(fd)
    return {}


@contextmanager
def chdir(folder):
    dir = os.getcwd()
    os.chdir(folder)
    yield
    os.chdir(dir)


@contextmanager
def silence():
    sys.stdout = devnull
    yield
    sys.stdout = sys.__stdout__


def make_parser(func):
    parser = argparse.ArgumentParser(description=func.__doc__)
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('folder', nargs='*', default=['.'])
    return parser


def run(func, *args):
    parser = make_parser(func)
    args2 = parser.parse_args()
    for folder in args2.folder:
        if args2.verbose:
            parprint(folder, end=' ')
        with chdir(folder):
            func(*args)
    if args2.verbose:
        parprint()
