# creates: H-WS2-band-structure.png
import matplotlib.pyplot as plt
import ase.db
from ase.dft.band_structure import BandStructure

# Connect to database
db = ase.db.connect('c2dm.db')
row = db.get(formula='WS2', prototype='MoS2')
dct = row.data.bs_pbe
ef = dct['efermi']
bs = BandStructure(row.cell, dct['path'], 0, reference=ef)
ax = bs.plot_with_colors(energies=dct['eps_so_mk'], colors=dct['sz_mk'],
                         emin=-2.5, emax=2.5,
                         ylabel='energy vs vacuum [eV]',
                         show=False)
ax.set_title('Band structure of H-WS$_2$ (PBE+SOC)')
plt.savefig('H-WS2-band-structure.png')
