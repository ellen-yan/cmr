import argparse

from ase.io import read
from ase.parallel import parprint


def calc(atoms, **parms):
    # run over the steps
    parprint("Initial relaxation...")
    from c2dm.relax import relax
    lparms = {k: v for k, v in parms.items() if k == 'ecut' or k == 'vacuum'}
    atoms = relax(atoms, **lparms)

    # continue? hform < 1 eV
    from c2dm.references import read_references, formation_energy
    hform = formation_energy(atoms, read_references()) / len(atoms)
    if hform > 1:
        parprint("NOT stable (hform)!")
        return

    parprint("Stability...")
    from c2dm.stability import relax2x2
    lparms = {k: v for k, v in parms.items() if k == 'ecut' or k == 'stdev'}
    relax2x2(**lparms)

    # continue? dyn stab < 10 meV, rmsd < 0.05 A
    from c2dm.rmsd_monolayer import calculate_rmsd
    e1 = read('ref_2x2.traj').get_potential_energy()
    e2 = read('relax-spin-paired_2x2.traj').get_potential_energy()
    s2 = read('relax-spin-paired_2x2.traj')
    if read('relax-spin-polarized_2x2.traj').get_potential_energy() < e2:
        e2 = read('relax-spin-polarized_2x2.traj').get_potential_energy()
        s2 = read('relax-spin-polarized_2x2.traj')
    d2 = calculate_rmsd(read('ref_2x2.traj'), s2)[0]
    if hform > 1 or (e2 - e1) / len(s2) > 10e-3 or d2 > 0.05:
        parprint("NOT stable (dyn stable)!")
        return

    parprint("PBE electronic properties...")
    from c2dm.bandstructure import bandstructure
    from c2dm.ehmasses import runeh
    from c2dm.fermi_surface import fermi_surface
    from c2dm.pdos import run_pdos
    from c2dm.anisotropy import anisotropy
    from c2dm.strains import run_strains
    from c2dm.polarizability import polarizability
    from c2dm.gllbsc import gllbsc
    from c2dm.hse import runhse

    scripts = [bandstructure,
               runeh,
               fermi_surface,
               run_strains,
               run_pdos,
               anisotropy,
               polarizability,
               gllbsc,
               runhse]

    names = ["Bandstructure",
             "Electron hole masses",
             "Fermi surface",
             "Strain calculations",
             "PDOS",
             "Anisotropy",
             "Polarizability",
             "GLLB",
             "HSE"]
    for script, name in zip(scripts, names):
        try:
            parprint("Starting {0} calculation.".format(name))
            script()
        except Exception as e:
            parprint("{0} failed!".format(name))
            parprint(e)

    # exclude time consuming steps
    # from c2dm.bb import bb
    # from c2dm.gw import gw_calc
    # from c2dm.bse import bse
    # bb()
    # gw_calc()
    # bse()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', nargs='?', default='guess.xyz',
                        help='Filename of input structure.')
    parser.add_argument('-e', '--ecut', default=800, help='cutoff energy')
    parser.add_argument('-v', '--vacuum', default=7.5, help='cutoff energy')
    parser.add_argument('--skip-long', action='store_true')
    args = parser.parse_args()

    atoms = read(args.filename)
    parms = dict(ecut=float(args.ecut), vacuum=float(args.vacuum))
    if args.skip_long:
        parms['stdev'] = 0.0
    calc(atoms, **parms)
