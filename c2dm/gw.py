import os

import numpy as np
from ase.db import connect
from ase.io import read
from ase.dft.kpoints import bandpath
from gpaw import GPAW
from gpaw.response.g0w0 import G0W0
from gpaw.response.gw_bands import GWbands
import gpaw.mpi as mpi
from c2dm.bandstructure import get_special_2d_path


def gw():
    path = os.path.basename(os.getcwd())
    formula, prototype = path.split('-')
    db = connect('../GW/c2dm-old.db')
    phase = {'MoS2': 'H', 'CdI2': 'T'}[prototype]

    row = db.get(xc='LDA', formula=formula, phase=phase)
    data = row.data
    eps_skn = data.qpbs_kn[np.newaxis]
    x = data.xqpbs_k
    np.savez('gw.npz',
             efermi=data.efermi,
             eps_skn=eps_skn,
             path=x,
             gap_gw=row.ind_gap_g0w0,
             dir_gap_gw=row.dir_gap_g0w0,
             cbm_gw=row.cbm_g0w0,
             vbm_gw=row.vbm_g0w0)


def gw_calc(kptdensity=5, ecut=150.0):
    """Calculate the gw bandstructure"""

    # get non-magnetic gs
    if not os.path.isfile('gs.gpw'):
        return
    else:
        calc = GPAW('gs.gpw', txt=None)
        if calc.get_spin_polarized():
            return

    # kpts
    def get_kpts_size(atoms, density):
        """trying to get a reasonable monkhorst size which hits high
        symmetry points
        """
        from gpaw.kpt_descriptor import kpts2sizeandoffsets as k2so
        size, offset = k2so(atoms=calc.atoms, density=density)
        size[2] = 1
        for i in range(2):
            if size[i] % 6 != 0:
                size[i] = 6 * (size[i] // 6 + 1)
        kpts = {'size': size, 'gamma': True}
        return kpts

    kpts = get_kpts_size(atoms=calc.atoms, density=kptdensity)

    # gs with wf
    calc.set(kpts=kpts,
             fixdensity=True,
             txt='gs_gw.txt')
    calc.get_potential_energy()
    calc.diagonalize_full_hamiltonian(ecut=ecut)
    calc.write('gs_gw.gpw', mode='all')

    # bands (-8,4)
    lb, ub = max(calc.wfs.nvalence // 2 - 8, 0), calc.wfs.nvalence // 2 + 4

    # gw...
    calc = G0W0(calc='gs_gw.gpw',
                bands=(lb, ub),
                ecut=ecut,
                ecut_extrapolation=True,
                truncation='2D',
                nblocksmax=True,
                anisotropy_correction=True,
                filename='g0w0',
                restartfile='g0w0.tmp',
                savepckl=True)
    calc.calculate()

    gw_results()


def gw_results():
    """get band strucutre (still not aligned to evac), to gw.npz"""
    cell = read('gs.gpw').cell
    kpoints = bandpath(get_special_2d_path(cell), cell=cell, npoints=0)[0]
    bands = GWbands(calc='gs_gw.gpw',
                    gw_file='g0w0_results.pckl',
                    kpoints=kpoints)

    results = bands.get_GW_bands(SO=False, nk_Int=50, interpolate=True,
                                 dft=False)
    eGW_kn = results['e_kn']
    ef = results['ef']
    N_occ = (eGW_kn[0] < ef).sum()
    gap = (eGW_kn[:, N_occ].min() - eGW_kn[:, N_occ - 1].max())
    dirgap = min(eGW_kn[:, N_occ] - eGW_kn[:, N_occ - 1])
    vbm = eGW_kn[:, N_occ - 1].max()
    cbm = eGW_kn[:, N_occ].min()
    np.savez('gw.npz',
             efermi=ef,
             eps_skn=eGW_kn,
             path=results['x_k'],
             gap_gw=gap,
             dir_gap_gw=dirgap,
             cbm_gw=vbm,
             vbm_gw=cbm)

    # remove large files, maybe more
    if mpi.world.rank in [0]:
        os.system('rm gs_gw.gpw')


if __name__ == '__main__':
    from c2dm import run
    run(gw_calc)
