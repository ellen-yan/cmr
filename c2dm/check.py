import argparse
import os
import sys

from ase.io import read
from c2dm import chdir
from c2dm.relax import relax_done


def check_relax(verbose=False):
    try:
        read('gs.gpw')
        gs = True
    except Exception:
        if verbose:
            print("check: No gs.gpw!")
        gs = False
    return (gs and
            relax_done('relax-spin-paired.traj', verbose=verbose) and
            relax_done('relax-spin-polarized.traj', verbose=verbose))


def check_relax2x2(verbose=False):
    try:
        read('gs_2x2.gpw')
        read('ref_2x2.traj').get_potential_energy()
        gs = True
    except Exception:
        if verbose:
            print("check: No gs_2x2.gpw or ref_2x2!")
        gs = False
    return (gs and
            relax_done('relax-spin-paired_2x2.traj', verbose=verbose) and
            relax_done('relax-spin-polarized_2x2.traj', verbose=verbose))


def check(verbose=False):
    return (check_relax(verbose) and
            check_relax2x2(verbose))
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('folder', nargs='+')
    args = parser.parse_args()

    for folder in args.folder:
        if not os.path.isdir(folder):
            continue
        with chdir(folder):
            print(folder, end=': ')
            if check(verbose=True):
                print("ok")
                # silence please...
                if not args.verbose:
                    sys.stdout.write("\033[F")
