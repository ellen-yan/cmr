import json

from ase.parallel import paropen
from gpaw import GPAW, FermiDirac


def gllbsc():
    calc = GPAW('densk.gpw', txt=None)
    atoms = calc.get_atoms()
    if atoms.get_initial_magnetic_moments().any():
        return

    dct = calc.todict()
    dct.update(fixdensity=False, txt='gllbsc.txt', xc='GLLBSC',
               occupations=FermiDirac(width=0.01))
    calc = GPAW(**dct)
    atoms.set_calculator(calc)
    atoms.get_potential_energy()
    response = atoms.calc.hamiltonian.xc.xcs['RESPONSE']
    response.calculate_delta_xc()
    Eks, deltaxc = response.calculate_delta_xc_perturbation()
    atoms.calc.write('gllbsc.gpw')
    v = atoms.calc.get_electrostatic_potential()
    evac = v[:, :, 0].mean()
    with paropen('gllbsc.json', 'w') as fd:
        json.dump({'deltaxc': deltaxc,
                   'evac': evac}, fd)


if __name__ == '__main__':
    from c2dm import run
    run(gllbsc)
