import numpy as np
from gpaw.occupations import occupation_numbers
from gpaw.symmetry import atoms2symmetry
from ase.units import Ha


def fermi_level(calc, eps_skn=None, nelectrons=None):
    """
    Parameters:
        calc: GPAW
            GPAW calculator
        eps_skn: ndarray, shape=(ns, nk, nb), optional
            eigenvalues (taken from calc if None)
        nelectrons: float, optional
            number of electrons (taken from calc if None)
    Returns:
        out: float
            fermi level
    """
    if nelectrons is None:
        nelectrons = calc.get_number_of_electrons()
    if eps_skn is None:
        eps_skn = eigenvalues(calc)
    eps_skn.sort(axis=-1)
    occ = calc.occupations.todict()
    weight_k = calc.get_k_point_weights()
    return occupation_numbers(occ, eps_skn, weight_k, nelectrons)[1] * Ha


def eigenvalues(calc):
    """
    Parameters:
        calc: Calculator
            GPAW calculator
    Returns:
        e_skn: (ns, nk, nb)-shape array
    """
    rs = range(calc.get_number_of_spins())
    rk = range(len(calc.get_ibz_k_points()))
    e = calc.get_eigenvalues
    return np.asarray([[e(spin=s, kpt=k) for k in rk] for s in rs])


def has_inversion(atoms):
    """
    Parameters:
        atoms: Atoms object
            atoms
    Returns:
        out: bool
    """
    atoms2 = atoms.copy()
    atoms2.pbc[:] = True
    atoms2.center(axis=2)
    return atoms2symmetry(atoms2).has_inversion


def interpolate_bandlines(calc, e_kn, kpoints, nk_int=50, verbose=False):
    """
    Interpolate along a band structure
    """
    from scipy.interpolate import InterpolatedUnivariateSpline

    from ase.utils import gcd
    from gpaw.kpt_descriptor import to1bz

    gd = calc.wfs.gd.new_descriptor()
    kd = calc.wfs.kd

    acell_cv = gd.cell_cv
    bcell_cv = 2 * np.pi * gd.icell_cv

    e_kn = np.sort(e_kn, axis=1)

    N_c = kd.N_c
    wpts_xc = kpoints

    # find points / path
    x_x = []
    k_xc = []
    k_x = []
    x = 0.
    X = []
    for nwpt in range(1, len(wpts_xc)):
        X.append(x)
        to_c = wpts_xc[nwpt]
        from_c = wpts_xc[nwpt - 1]
        vec_c = to_c - from_c
        Nv_c = (vec_c * N_c).round().astype(int)
        Nv = abs(gcd(gcd(Nv_c[0], Nv_c[1]), Nv_c[2]))
        if verbose:
            print('From', from_c, 'to', to_c, ':', Nv, 'points')
        dv_c = vec_c / Nv
        dv_v = np.dot(dv_c, bcell_cv)
        dx = np.linalg.norm(dv_v)
        if nwpt == len(wpts_xc) - 1:
            # X.append(Nv * dx)
            Nv += 1
        for n in range(Nv):
            k_c = from_c + n * dv_c
            bzk_c = to1bz(np.array([k_c]), acell_cv)[0]
            ikpt = kd.where_is_q(bzk_c, kd.bzk_kc)
            x_x.append(x)
            k_xc.append(k_c)
            k_x.append(ikpt)
            x += dx
    X.append(x_x[-1])
  
    # and energies
    k_ibz_x = np.zeros_like(k_x)
    eGW_kn = np.zeros((len(k_x), e_kn.shape[1]))
    for n in range(e_kn.shape[1]):
        for ik in range(len(k_x)):
            ibzkpt = kd.bz2ibz_k[k_x[ik]]
            k_ibz_x[ik] = ibzkpt
            eGW_kn[ik, n] = e_kn[ibzkpt, n]

    # now interpolate (per band)
    xfit_k = np.linspace(x_x[0], x_x[-1], nk_int - len(x_x))
    xfit_k = np.append(xfit_k, x_x)
    xfit_k = np.sort(xfit_k)
    nk_int = len(xfit_k)
  
    kfit_x = []
    for x in xfit_k:
        for i in range(len(x_x)):
            if x < x_x[i]:
                kw = k_xc[i] - k_xc[i - 1]
                l = (x - x_x[i - 1]) / (x_x[i] - x_x[i - 1])
                kfit_x.append(k_xc[i - 1] + kw * l)
                break
            if i == len(x_x) - 1:
                kfit_x.append(k_xc[i])
    kfit_x = np.array(kfit_x)

    efit_kn = np.zeros((nk_int, eGW_kn.shape[1]))
    for n in range(eGW_kn.shape[1]):
        fit_e = InterpolatedUnivariateSpline(x_x, eGW_kn[:, n])
        efit_kn[:, n] = fit_e(xfit_k)

    results = {'x_k': xfit_k,
               'X': X,
               'path': kfit_x,
               'e_kn': efit_kn}

    return results
