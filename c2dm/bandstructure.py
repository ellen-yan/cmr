import os

import numpy as np
from gpaw import GPAW
import gpaw.mpi as mpi
from gpaw.spinorbit import get_spinorbit_eigenvalues as get_soc_eigs
from ase.io import read
from ase.geometry import crystal_structure_from_cell as csfc
from ase.parallel import paropen


special_2d_paths = {'hexagonal': 'GMKG',
                    'orthorhombic': 'GXSYGS',
                    'tetragonal': 'MGXM',
                    # ...
                    }


def get_special_2d_path(cell):
    return special_2d_paths[csfc(cell)]


def gs_done():
    return os.path.isfile('gs.gpw')


def bs_done():
    return os.path.isfile('bs.gpw')


def bandstructure(kptpath=None, npoints=400, emptybands=20):
    """Calculate the bandstructure based on a relaxed structure in gs.gpw."""

    if os.path.isfile('eigs_spinorbit.npz'):
        return
    if not gs_done():
        return
    if not bs_done():
        if kptpath is None:
            cell = read('gs.gpw').cell
            kptpath = get_special_2d_path(cell)
        convbands = emptybands // 2
        parms = {'basis': 'dzp',
                 'nbands': -emptybands,
                 'txt': 'bs.txt',
                 'fixdensity': True,
                 'kpts': {'path': kptpath, 'npoints': npoints},
                 'convergence': {'bands': -convbands},
                 'symmetry': 'off'}

        calc = GPAW('gs.gpw',
                    **parms)

        calc.get_potential_energy()
        calc.write('bs.gpw')

    calc = GPAW('bs.gpw', txt=None)
    # stuff below could be moved to the collect script.
    # vacuum level
    vh = calc.get_electrostatic_potential()
    e_vac1, e_vac2 = vh.mean(axis=0).mean(axis=0)[[0, -1]]
    dipole = calc.get_dipole_moment()
    with paropen('dipole.txt', 'w') as f:
        f.write('{}'.format(dipole))
    assert abs(e_vac1 - e_vac2) < 1.0e-2
    with paropen('evac.txt', 'w') as f:
        f.write('{}'.format(e_vac1))
    with paropen('evac2.txt', 'w') as f:
        f.write('{}'.format(e_vac2))

    # spinorbit eigenvalues only works in serial
    ranks = [0]
    comm = mpi.world.new_communicator(ranks)
    if mpi.world.rank in ranks:
        calc = GPAW('bs.gpw', communicator=comm, txt=None)
        e_mk, s_kvm = get_soc_eigs(calc,
                                   return_spin=True,
                                   bands=range(calc.get_number_of_bands()))
        s_mvk = s_kvm.transpose(2, 1, 0)
        with paropen('eigs_spinorbit.npz', 'wb') as f:
            np.savez(f, e_mk=e_mk, s_mvk=s_mvk)


if __name__ == '__main__':
    from c2dm import run
    run(bandstructure)
