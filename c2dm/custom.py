import matplotlib.pyplot as plt
import numpy as np
from ase.db.web import creates
from ase.dft.band_structure import BandStructure
from ase.dft.kpoints import labels_from_kpts
from ase.units import Ha, Bohr, alpha
from c2dm.bz import plot_bz


title = 'Computational 2D materials database'

key_descriptions = {
    'prototype': ('Prototype', 'Structure prototype', ''),
    'class': ('Class', 'Class of material', ''),
    'hform': ('Heat of formation', '', 'eV'),
    'cbm': ('CBM', 'Conduction band minimum vs vacuum (PBE)', 'eV'),
    'cbm_nosoc': ('CBM', 'Conduction band minimum vs vacuum (PBE)', 'eV'),
    'vbm': ('VBM', 'Valence band maximum vs vacuum (PBE)', 'eV'),
    'vbm_nosoc': ('VBM', 'Valence band maximum vs vacuum (PBE)', 'eV'),
    'cbm_gllbsc': ('CBM', 'Conduction band minimum vs vacuum (GLLBSC)', 'eV'),
    'vbm_gllbsc': ('VBM', 'Valence band maximum vs vacuum (GLLBSC)', 'eV'),
    'cbm_hse': ('CBM', 'Conduction band minimum vs vacuum (HSE)', 'eV'),
    'vbm_hse': ('VBM', 'Valence band maximum vs vacuum (HSE)', 'eV'),
    'vbm_gw': ('VBM', 'Valence band maximum vs vacuum (GW)', 'eV'),
    'cbm_gw': ('VBM', 'Conduction band maximum vs vacuum (GW)', 'eV'),
    'gap': ('Band gap (PBE)', '', 'eV'),
    'gap_nosoc': ('Band gap (PBE)', '', 'eV'),
    'dir_gap': ('Direct band gap (PBE)', '', 'eV'),
    'dir_gap_nosoc': ('Direct band gap (PBE)', '', 'eV'),
    'gap_gw': ('Band gap (GW)', '', 'eV'),
    'dir_gap_gw': ('Direct band gap (GW)', '', 'eV'),
    'gap_gllbsc': ('Fundamental gap (GLLBSC)', '', 'eV'),
    'dir_gap_gllbsc': ('Direct band gap (GLLBSC)', '', 'eV'),
    'gap_hse': ('Band gap (HSE)', '', 'eV'),
    'dir_gap_hse': ('Direct band gap (HSE)', '', 'eV'),
    'emass1': ('Electron mass (1)', 'Electron mass (direction 1)',
               '`m_e`'),
    'emass2': ('Electron mass (2)', 'Electron mass (direction 2)',
               '`m_e`'),
    'hmass1': ('Hole mass (1)', 'Hole mass (direction 1)', '`m_e`'),
    'hmass2': ('Hole mass (2)', 'Hole mass (direction 2)', '`m_e`'),
    'emass1_nosoc': ('Electron mass (1)',
                     'Electron mass (direction 1) without SOC',
                     '`m_e`'),
    'emass2_nosoc': ('Electron mass (2)',
                     'Electron mass (direction 2) without SOC',
                     '`m_e`'),
    'hmass1_nosoc': ('Hole mass (1)',
                     'Hole mass (direction 1) without SOC',
                     '`m_e`'),
    'hmass2_nosoc': ('Hole mass (2)',
                     'Hole mass (direction 2) without SOC',
                     '`m_e`'),
    'q2d_macro_df_slope': ('Dielectic func',
                           'Slope of macroscopic 2D static dielectric '
                           'function at q=0', '?'),
    'is_magnetic': ('Magnetic', '', ''),
    'is_dir_gap': ('Direct gap (PBE)', '', ''),
    'is_metallic': ('Metallic', '', ''),
    'maganis': (
        'Mag. Anis.',
        'Difference between out of plane and inplane mangetization energy',
        'meV'),
    'work_function': ('Work function', '', 'eV'),
    'dos': ('DOS', 'Density of states at Fermi level', '`\\text{Ang}^{-3}`'),
    'is_dyn_stable': ('Dyn. stable', 'Dynamically stable', ''),
    'e_distortion': ('Dist. energy', 'Distortion energy per atom', 'eV'),
    'r_distortion': ('Dist. distance', 'Distortion distance change (max)',
                     'Ang'),
    'stability': ('Stability',
                  'Heat of formation and dynamic stability is tested', ''),
    'e_magnetic': ('Magn. energy', 'Energy change due to magnetic solution',
                   'eV'),
    'c_11': ('Bulk modulus (x)',
             'Bulk modulus in the xx direction',
             'N/m'),
    'c_22': ('Bulk modulus (y)',
             'Bulk modulus in the yy direction',
             'N/m'),
    'bulk_modulus': ('Bulk modulus', '', 'N/m'),
    'speed_of_sound_x': ('Speed of sound (x)', '', 'm/s'),
    'speed_of_sound_y': ('Speed of sound (y)', '', 'm/s'),
    'speed_of_sound': ('Speed of sound', '', 'm/s'),
    'D_vbm_nosoc': ('Deformation Pot. (VBM)',
                    'Deformation potential at VBM w/o SOC',
                    'eV'),
    'D_cbm_nosoc': ('Deformation Pot. (CBM)',
                    'Deformation potential at CBM w/o SOC',
                    'eV'),
    'D_vbm': ('Deformation Pot. (VBM)',
              'Deformation potential at VBM',
              'eV'),
    'D_cbm': ('Deformation Pot. (CBM)',
              'Deformation potential at CBM',
              'eV'),
    'spacegroup': ('Spacegroup',
                   'International space group short symbol',
                   '')}

default_columns = ['formula', 'prototype', 'spacegroup', 'hform', 'gap',
                   'magmom', 'work_function']

special_keys = [
    ('SELECT', 'prototype'),
    ('SELECT', 'class'),
    ('BOOL', 'is_magnetic'),
    ('RANGE', 'gap', 'Band gap range [eV]',
     [('PBE', 'gap'),
      ('G0W0@PBE', 'gap_gw'),
      ('GLLBSC', 'gap_gllbsc'),
      ('HSE@PBE', 'gap_hse')])]

basic = ('Item', ['prototype', 'hform', 'spacegroup',
                  'gap', 'dir_gap', 'gap_gllbsc', 'work_function',
                  'magmom', 'maganis',
                  'c_11', 'c_22', 'bulk_modulus',
                  'speed_of_sound_x', 'speed_of_sound_y', 'speed_of_sound'])
pbe = ('Band energies (PBE)',
       ['work_function', 'dos', 'gap', 'dir_gap', 'vbm', 'cbm'])
gw = ('Band energies (G0W0@PBE)', ['gap_gw', 'dir_gap_gw', 'vbm_gw', 'cbm_gw'])
gllbsc = ('Band energies (GLLB-SC)',
          ['work_function_gllbsc', 'dos_gllbsc', 'gap_gllbsc',
           'dir_gap_gllbsc', 'vbm_gllbsc', 'cbm_gllbsc'])
hse = ('Band energies (HSE@PBE)',
       ['work_function_hse', 'dos_hse', 'gap_hse', 'dir_gap_hse',
        'vbm_hse', 'cbm_hse'])


mass = ('Masses (PBE)', ['emass1', 'emass2', 'hmass1', 'hmass2'])

deformation_potential = ('Deformation Potentials (PBE)',
                         ['D_vbm', 'D_cbm'])

opt = ('Constants', ['alpha'])

layout = [('Basic properties',
           [[basic, 'CELL'],
            ['ATOMS']]),
          ('Electronic band structure (PBE)',
           [['pbe-bs.png', 'bz.png'],
            ['pbe-pdos.png', pbe,
             mass, deformation_potential]]),
          ('Electronic band structure (HSE)',
           [['hse-bs.png'],
            [hse]]),
          ('Electronic band structure (GW)',
           [['gw-bs.png'],
            [gw]]),
          ('Optical properties',
           [[opt, 'abs-out.png', 'rpa-pol-out.png'],
            ['abs-in.png', 'rpa-pol-in.png']])]


data_description = {
    'bs_pbe': {
        'path': 'BZ path: (npoints, 3)-array',
        'eps_skn': 'pbe eigenvalues: (nspin, npoints, nbands)-array',
        'eps_so_mk': 'spin orbit eigenvalues: (2 x nband, npoints)-array',
        'sz_mk': 'colors: (2 x nband, npoints)-array',
        'kvbm': 'VBM k-point with SOC: (3,)-array',
        'kcbm': 'CBM k-point with SOC: (3,)-array',
        'kvbm_nosoc': 'VBM k-point without SOC: (3,)-array',
        'kcbm_nosoc': 'CBM k-point without SOC: (3,)-array'},
    'bs_gw':
        {'path': 'BZ path: str',
         'efermi': 'Fermi-level',
         'eps_skn': 'quasi-particle eigenvalues'},
    'bs_gllbsc':
        {'path': 'BZ path: (npoints, 3)-array',
         'efermi': 'Fermi-level',
         'eps_skn': 'quasi-particle eigenvalues'},
    'bs_hse':
        {'path': 'BZ path: (npoints, 3)-array',
         'kptpath': 'BZ path: str',
         'efermi': 'Fermi-level',
         'eps_skn': 'HSE eigenvalues: (nspin, npoints, nbands)-array',
         'kvbm': 'VBM k-point with SOC: (3,)-array',
         'kcbm': 'CBM k-point with SOC: (3,)-array',
         'kvbm_nosoc': 'VBM k-point without SOC: (3,)-array',
         'kcbm_nosoc': 'CBM k-point without SOC: (3,)-array'},
    'bse_abs': {'freq': 'frequency',
                'par': 'absorbtion in plane',
                'per': 'absorbtion out of plane'},
    'rpa_abs': {'freq': 'frequency',
                'par': 'absorbtion in plane',
                'per': 'absorbtion out of plane'},
    'rpa_pol': {'freq': 'frequency',
                'par': 'polarizability in plane'},
    'pdos_pbe_nosoc': {
        'pdos_sal': 'PDOS (nspin,)-list of dicts, \
                    i.e. pdos_sal[spin: int][a: int][l: str]',
        'energies_s': 'energies (ns, npoints)-list',
        'efermi': 'Fermi-level: float'}
}

params = {'legend.fontsize': 'large',
          'axes.labelsize': 'large',
          'axes.titlesize': 'large',
          'xtick.labelsize': 'large',
          'ytick.labelsize': 'large'}
plt.rcParams.update(**params)


def add_bs_pbe(row, ax):
    """plot pbe with soc on ax
   """
    c = '0.8'  # light grey for pbe with soc plot
    ls = '--'
    lw = 1.5
    d = row.data.bs_pbe
    kpts = d['path']
    e_mk = d['eps_so_mk']
    xcoords, label_xcoords, labels = labels_from_kpts(kpts, row.cell)
    for e_k in e_mk[:-1]:
        ax.plot(xcoords, e_k, color=c, ls=ls, lw=lw, zorder=0)

    ax.plot(xcoords, e_mk[-1], color=c, ls=ls, lw=lw, label='PBE', zorder=0)
    return ax


@creates('pbe-pdos.png')
def pdos_pbe(row):
    if 'pdos_pbe' not in row.data:
        return
    colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
    dct = row.data.pdos_pbe
    e = dct['energies']
    pdos_sal = dct['pdos_sal']
    spinpol = (len(pdos_sal) == 2)
    ef = dct['efermi']
    ax = plt.figure().add_subplot(111)
    emin = row.get('vbm', ef) - 2
    emax = row.get('cbm', ef) + 2
    i1, i2 = abs(e - emin).argmin(), abs(e - emax).argmin()
    pmax = 0.0
    for s, pdos_al in sorted(pdos_sal.items()):
        c = 0
        sign = 1 if s == 0 else -1
        for a, pdos_l in sorted(pdos_al.items()):
            for l, pdos in sorted(pdos_l.items(), reverse=True):
                if s == 0:
                    label = '{} ({})'.format(row.symbols[a], l)
                else:
                    label = None
                pmax = max(pmax, pdos[i1: i2].max())
                ax.plot(pdos * sign, e,
                        label=label, color=colors[c])

                c += 1

    ax.legend()
    ax.axhline(ef, color='k')
    ax.set_ylim(emin, emax)
    if spinpol:
        ax.set_xlim(-pmax, pmax)
    else:
        ax.set_xlim(0, pmax)

    ax.set_xlabel('projected dos [states / eV]')
    ax.set_ylabel('energy vs vacuum [eV]')
    plt.savefig('pbe-pdos.png')


@creates('gw-bs.png')
def bs_gw(row):
    if 'bs_gw' not in row.data:
        return

    d = row.data.bs_pbe
    e = d['eps_so_mk'].T[np.newaxis]
    ef = d['efermi']
    bs = BandStructure(row.cell, d['path'], e, ef)
    emin = row.get('vbm', ef) - ef - 2
    emax = row.get('cbm', ef) - ef + 2
    ax = bs.plot(ls='--', show=False,
                 emin=emin, emax=emax,
                 ylabel='energy vs vacuum [eV]', label='PBE+SOC')

    gw = row.data.bs_gw
    e = gw['eps_skn']
    x = gw['path']
    bs.energies = e
    bs.xcoords = x * bs.xcoords[-1] / x[-1]
    bs.plot(ax, show=False, color='b', label='GW+SOC')
    plt.savefig('gw-bs.png')


@creates('hse-bs.png')
def bs_hse(row):
    if 'bs_hse' not in row.data:
        return
    d = row.data.bs_hse
    if 'eps_mk' in d:  # plot with soc if available
        eps_mk = d['eps_mk']
        kpts = d['path']
        ef = d['efermi']
        emin = row.get('vbm', ef) - ef - 2
        emax = row.get('cbm', ef) - ef + 2
        bs = BandStructure(row.cell, kpts, eps_mk.T[np.newaxis], ef)
        ax = bs.plot(ls='-', color='darkcyan', show=False,
                     emin=emin, emax=emax, ylabel='energy vs vacuum [eV]')
        if 'bs_pbe' in row.data:
            ax = add_bs_pbe(row, ax)
        ax.legend()
        plt.savefig('hse-bs.png')
    else:  # fallback without soc
        d = row.data.bs_hse
        e = d['eps_skn']
        ef = d['efermi_nosoc']
        emin = row.get('vbm', ef) - ef - 2
        emax = row.get('cbm', ef) - ef + 2
        bs = BandStructure(row.cell, d['path'], e, ef)
        ax = bs.prepare_plot(emin=emin, emax=emax,
                             ylabel='energy vs vacuum [eV]')
        # add PBE with soc band structure
        if 'bs_pbe' in row.data:
            ax = add_bs_pbe(row, ax)
        # plot HSE
        ax = bs.plot(show=False, ax=ax, emin=emin, emax=emax,
                     label='HSE w/o soc')
        plt.savefig('hse-bs.png')


@creates('pbe-bs.png')
def bs_pbe(row):
    if 'eps_so_mk' not in row.data.bs_pbe:
        return
    d = row.data.bs_pbe
    e = d['eps_skn']
    kpts = d['path']
    ef = d['efermi']
    emin = row.get('vbm', ef) - ef - 2
    emax = row.get('cbm', ef) - ef + 2
    bs = BandStructure(row.cell, kpts, e, ef)
    # pbe without soc
    nosoc_style = dict(colors=['0.8'] * e.shape[0],
                       label='PBE w/o SOC',
                       ls='--',
                       lw=1.5,
                       zorder=0)
    ax = bs.plot(ax=None, show=False, emin=emin, emax=emax,
                 ylabel='energy vs vacuum [eV]',
                 **nosoc_style)
    # pbe with soc
    e_mk = d['eps_so_mk']
    sz_mk = d['sz_mk']

    ax = bs.plot_with_colors(ax=ax, energies=e_mk, colors=sz_mk,
                             filename='pbe-bs.png', show=False,
                             sortcolors=True, loc='upper right')


@creates('bz_nosoc.png')
def bz_nosoc(row):
    plot_bz(row, soc=False, fname='bz_nosoc.png')


@creates('bz.png')
def bz_soc(row):
    plot_bz(row, soc=True, fname='bz.png')


@creates('abs-in.png', 'abs-out.png')
def absorption(row):
    def xlim():
        return (0, 5)

    def ylim(freq, data):
        x1, x2 = xlim()
        i2 = abs(freq - x2).argmin()
        return (0, data[:i2].max())

    def pol2abs(frequencies, pol):
        x = 4 * np.pi * frequencies * alpha / Ha / Bohr
        return x * pol.imag

    if 'bse_abs' in row.data or 'absorptionspectrum' in row.data:
        ax = plt.figure().add_subplot(111)
        if 'bse_abs' in row.data:
            a = row.data.bse_abs
            ax.plot(a.freq, a.par, label='BSE')
        if 'absorptionspectrum' in row.data:
            freq = row.data.absorptionspectrum.frequencies
            abs_in = pol2abs(freq, row.data.absorptionspectrum.alphax_w)
            ax.plot(freq, abs_in, label='RPA')
            ax.set_ylim(ylim(freq, abs_in))
        ax.set_title('in plane')
        ax.set_xlabel('energy [eV]')
        ax.set_ylabel('absorbance')
        ax.legend()
        ax.set_xlim(xlim())
        plt.savefig('abs-in.png')
        ax = plt.figure().add_subplot(111)
        if 'bse_abs' in row.data:
            a = row.data.bse_abs
            ax.plot(a.freq, a.per, label='BSE')
        if 'absorptionspectrum' in row.data:
            freq = row.data.absorptionspectrum.frequencies
            abs_out = pol2abs(freq, row.data.absorptionspectrum.alphaz_w)
            ax.plot(freq, abs_out, label='RPA')
            ax.set_ylim(ylim(freq, abs_out))
        ax.set_title('out of plane')
        ax.set_xlabel('energy [eV]')
        ax.set_ylabel('absorbance')
        ax.legend()
        ax.set_xlim(xlim())
        plt.savefig('abs-out.png')


@creates('rpa-pol-in.png', 'rpa-pol-out.png')
def polarizability(row):
    def ylims(ws, data, wstart=0.0):
        i = abs(ws - wstart).argmin()
        x = data[i:]
        x1, x2 = x.real, x.imag
        y1 = min(x1.min(), x2.min())
        y2 = max(x1.max(), x2.max())
        return y1, y2

    if 'absorptionspectrum' in row.data:
        data = row.data['absorptionspectrum']
        frequencies = data['frequencies']
        i2 = abs(frequencies - 10.0).argmin()
        frequencies = frequencies[:i2]
        alphax_w = data['alphax_w'][:i2]
        alphaz_w = data['alphaz_w'][:i2]

        ax = plt.figure().add_subplot(111)
        ax.plot(frequencies, np.real(alphax_w), label='real')
        ax.plot(frequencies, np.imag(alphax_w), label='imag')
        ax.set_title('in plane')
        ax.set_xlabel('energy [eV]')
        ax.set_ylabel('polarizability')
        ax.set_ylim(ylims(ws=frequencies, data=alphax_w, wstart=0.5))
        ax.legend()
        plt.savefig('rpa-pol-in.png')

        ax = plt.figure().add_subplot(111)
        ax.plot(frequencies, np.real(alphaz_w), label='real')
        ax.plot(frequencies, np.imag(alphaz_w), label='imag')
        ax.set_title('out of plane')
        ax.set_xlabel('energy [eV]')
        ax.set_ylabel('polarizability')
        ax.set_ylim(ylims(ws=frequencies, data=alphaz_w, wstart=0.5))
        ax.legend()
        plt.savefig('rpa-pol-out.png')
