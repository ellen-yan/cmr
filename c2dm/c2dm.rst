.. _c2dm:

============
2D materials
============

.. contents::

This database contains calculated structural and electronic properties of a
range of 2D materials. The database contains the results presented in the
following paper:

.. container:: article

    Rasmussen, F., Thygesen, K. S.

    `Computational 2D Materials Database: Electronic Structure of
    Transition-Metal Dichalcogenides and Oxides`__

    J. Phys. Chem. C, 2015, 119 (23), pp 13169–13183, April 30, 2015

    __ http://pubs.acs.org/doi/abs/10.1021/acs.jpcc.5b02950

The data can be downloaded or browsed online as shown below.

.. Let's not show this links right now:
.. * `Browse data <https://cmrdb.fysik.dtu.dk/?project=c2dm>`_
.. * Download raw data: :download:`c2dm.db`

* Download reference state data: :download:`references.json`
* Download old data: :download:`c2dm-old.db`


Electronic structure of 2D materials
====================================

The structures were first relaxed using the PBE xc-functional and a 18x18x1
k-point sampling until all forces on the atoms where below 0.01 eV/Å. The rows
with xc='PBE' contains data from these calculations.

For materials that were found to be semiconducting in the PBE calculations we
furthermore performed calculations using the LDA and GLLB-SC xc functionals
and the lattice constants and atom positions found from the PBE calculation.
For these calculations we used a 30x30x1 k-point sampling. For the GLLB-SC
calculations we calculated the derivative discontinuity and have added this
contribution to the electronic band gaps. Data for these calculations are
found in rows with xc='GLLBSC' and xc='LDA', respectively.

Furthermore, we calculated the G0W0 quasiparticle energies using the
wavefunctions and eigenvalues from the LDA calculations and a plane-wave
cut-off energy of at least 150 eV. The quasiparticle energies where further
extrapolated to infinite cut-off energy via the methods described in the
paper. The LDA rows thus further have key-value pairs with the results from
the G0W0 calculations.


Key-value pairs
---------------

.. csv-table::
    :file: keytable.csv
    :header-rows: 1
    :widths: 3 10 1


Examples
--------

The following python script shows how to plot the positions of the VBM and CBM.

.. literalinclude:: plot_band_alignment.py

This produces the figure

.. image:: band-alignment.png

This script plots the band structure of WS2.

.. literalinclude:: plot_band_structure.py

.. image:: H-WS2-band-structure.png


Tools for creating the "Computational 2D materials" database
============================================================


Requirements
------------

* Python_ 3.4-3.6
* ASE_ 3.14?
* GPAW_ 1.3?


Installation
------------

Clone the code from https://gitlab.com/camd/cmr and tell Python where to find
it::

    $ cd ~
    $ git clone git@gitlab.com:camd/cmr
    $ export PYTHONPATH=~/cmr:$PYTHONPATH


Workflow
--------

Here are some of the steps (module names in parenthesis):

* Structure optimization (``c2dm.relax``)
* Check for local distortions, phonon calculation?
* Heat of formation and reference states (``c2dm.references``)
* Magnetic anisotropy (``c2dm.anisotropy``)
* PBE effective masses and gaps (``c2dm.ehmasses``, ``c2dm.gap``)
* PBE band structure (``c2dm.bandstructure``)
* HSE band gap (``c2dm.hse``)
* GLLBSC band gap (``c2dm.gllbsc``)
* GW band structure (``c2dm.gw``)
* Dielectric building block (``c2dm.bb``)
* RPA absorbance (``c2dm.bb``)
* Static polarizability (``c2dm.bb``)
* BSE absorbance (``c2dm.bse``)


Example (relaxation)
--------------------

.. automodule:: c2dm.relax
    :members: relax

::

    from ase.io import read
    from c2dm.relax import relax

    slab = read('mos2.json')
    relax(slab)

or ::

    $ python3 -m c2dm.relax --help  # for help
    $ python3 -m c2dm.relax -p MoS2 -c mos2.json

Run via *sbatch* like this::

    $ gpaw sbatch -- -p xeon8 -n 8 python3 -m c2dm.relax -p MoS2 -c mos2.json


Band-structure
--------------

Run band-structure step for all folders matching ``*-`` (Formula-Prototype)::

    $ for folder in *-*
    > do
    > gpaw sbatch -- -pxeon8 -n8 python3 -m c2dm.bandstructure $folder
    > done


Example (heat of formation)
---------------------------

::

    from ase.io import read
    from c2dm.references import read_references, formation_energy as fe
    slab = read('mos2.json')
    references = read_references()
    e = fe(slab, references)

or

::

    $ python3 -m c2dm.references mos2.json
    E(MoS2) - 1*E(Mo) + 2*E(S) = -0.842 eV


.. _Python: http://www.python.org/
.. _ASE: http://wiki.fysik.dtu.dk/ase/
.. _GPAW: http://wiki.fysik.dtu.dk/gpaw/
