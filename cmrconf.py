# import subprocess

names = [
    'dssc',
    # 'molecules', 'solids',
    # 'dcdft', 'dcdft_gpaw_pw_paw09'
    'mp_gllbsc', 'organometal', 'cubic_perovskites',
    'low_symmetry_perovskites',
    'c2dm',
    'tmfp06d', 'absorption_perovskites',
    'funct_perovskites', 'fcc111', 'compression', 'g2', 'catapp']

ASE_DB_NAMES = []
for db in names:
    # url = 'postgresql://ase:ase@localhost:5432/' + db
    url = db + '/' + db + '.db'
    py = db + '/custom.py'
    ASE_DB_NAMES.extend([url, py])
    # subprocess.check_call('python3 -m ase db {} -i all.db -k project={}'
    #                       .format(url, db), shell=True)
    # print('create database {};'.format(db))
    # print('ase db {} -i postgresql://ase:ase@localhost:5432/{}'
    #       .format(url, db))

ASE_DB_HOMEPAGE = 'https://cmr.fysik.dtu.dk/'
