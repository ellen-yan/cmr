title = 'Solar'

key_descriptions = {}


special_keys = [
    ('SELECT', 'Unit'),
    ('SELECT', 'fold')]

default_columns = ['id', 'formula', 'KS_gap', 'fold', 'eta', 'rho',
                   'E_gap', 'dip', 'Unit', 'DeltaU']
